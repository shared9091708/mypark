package com.divy.mypark.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.api.responsemodel.ParkObjects
import com.divy.mypark.custom.hide
import com.divy.mypark.custom.show
import com.divy.mypark.databinding.ListItemParkObjectsBinding
import com.bumptech.glide.Glide

class ParkObjectsListRvAdapter(
    private val click : (parkObject:ParkObjects) -> Unit,
    private val list: ArrayList<ParkObjects>,
    private val totalCount: Int) :
    RecyclerView.Adapter<ParkObjectsListRvAdapter.ViewHolder>(){

    inner class ViewHolder(private val binding: ListItemParkObjectsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(parkObjects: ParkObjects) {
            binding.apply {
                if (isObjectFound(parkObjects.count)){
                    Glide.with(binding.root.context).load(parkObjects.objectImage).into(ivImage2)
                    mainCard1.hide()
                    mainCard2.show()
                    infoButton.show()
                    tvObjectName2.text = parkObjects.objectName
                    tvObjectIndigenousName2.text = "("+parkObjects.objectIndigenousName+")"
                    tvCount.text = parkObjects.count.toString()+"/"+totalCount
                    infoButton.setOnClickListener {
                        click.invoke(parkObjects)
                    }
                }else{
                    mainCard2.hide()
                    infoButton.hide()
                    mainCard1.show()
                    tvObjectName.text = parkObjects.objectName
                }
            }
        }
    }

    private fun isObjectFound(count:Int?):Boolean{
        return if (count!=null){
            count.toInt()>=1
        }else{
            false
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ListItemParkObjectsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
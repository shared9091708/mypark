package com.divy.mypark.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.databinding.ListItemQuizBinding
import com.divy.mypark.model.QuizModel
import com.google.gson.Gson

class QuizAdapter(
    private val list: ArrayList<QuizModel>,
    private val click : (answer:String,position:Int) -> Unit,
) :
    RecyclerView.Adapter<QuizAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: ListItemQuizBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(model: QuizModel) {
            binding.apply {
                Log.e("ClickAnswer", "ADAPTER MODEL "+ Gson().toJson(model))

                tvAnswer.text = model.answer
                mainCard.setCardBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        model.color
                    )
                )

                mainCard.setOnClickListener {
                    Log.e("ClickAnswer", "adapter Click")
                    if (model.isClickEnabled) {
                        click.invoke(model.answer, absoluteAdapterPosition)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ListItemQuizBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(list[position])
    }
}
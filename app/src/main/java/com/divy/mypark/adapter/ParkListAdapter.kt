package com.divy.mypark.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.databinding.ListItemSelectParksBinding
import com.bumptech.glide.Glide

class ParkListAdapter(
    private val click : (parks:Parks) -> Unit,
    private val listOfParks: ArrayList<Parks>) :
    RecyclerView.Adapter<ParkListAdapter.ViewHolder>(){

    inner class ViewHolder(private val binding: ListItemSelectParksBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(park: Parks) {
            binding.apply {
                Glide.with(binding.root.context).load(park.parkImage).into(binding.ivParkImage)
                tvParkName.text = park.parkName
                mainCard.setOnClickListener {
                    click.invoke(park)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ListItemSelectParksBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(listOfParks[position])
    }

    override fun getItemCount(): Int {
        return listOfParks.size
    }
}
package com.divy.mypark.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.databinding.LayoutParksBinding
import com.divy.mypark.model.ParkModel

class ParkAdapter(
    private val list: MutableList<ParkModel>,
    var activity: FragmentActivity,
) :
    RecyclerView.Adapter<ParkAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: LayoutParksBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(list: ParkModel) {
            binding.ivpark.setImageResource(list.image)
            binding.tvParkname.text = list.parkName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutParksBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(list[position])
    }
}
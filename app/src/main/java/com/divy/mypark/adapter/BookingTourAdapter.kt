package com.divy.mypark.adapter

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.R
import com.divy.mypark.api.responsemodel.Tours
import com.divy.mypark.databinding.ListItemBookingBinding
import com.bumptech.glide.Glide

class BookingTourAdapter(
    private val onBookClick : (tour:Tours) -> Unit,
    private val onMoreInfoClick : (tour:Tours) -> Unit,
    private val listOfTours: ArrayList<Tours>) :
    RecyclerView.Adapter<BookingTourAdapter.ViewHolder>(){

    inner class ViewHolder(private val binding: ListItemBookingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(tour: Tours) {
            binding.apply {
                Glide.with(binding.root.context).load(tour.tourGuide?.profilePicture).into(ivGuideImage)
                Glide.with(binding.root.context).load(tour.tourImage).into(ivTourImage)


                val guideSpan = SpannableStringBuilder(binding.root.context.getString(R.string.guide))
                guideSpan.setSpan(
                    ForegroundColorSpan(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.colorPrimary
                        )
                    ), 0, binding.root.context.getString(R.string.guide).length, 0
                )
                guideSpan.setSpan(
                    StyleSpan(Typeface.BOLD),
                    0,
                    binding.root.context.getString(R.string.guide).length,
                    0
                )


                val guideName = tour.tourGuide?.firstName+""
                val guideNameSpan = SpannableStringBuilder(guideName)
                guideNameSpan.setSpan(
                    ForegroundColorSpan(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.colorPrimary
                        )
                    ), 0, guideName.length, 0
                )
                guideNameSpan.setSpan(StyleSpan(Typeface.NORMAL), 0, guideName.length, 0)

                tvGuideName.text = guideSpan.append(" ").append(guideNameSpan)

                val durationTextSpan = SpannableStringBuilder(binding.root.context.getString(R.string.duration))
                durationTextSpan.setSpan(
                    ForegroundColorSpan(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.colorPrimary
                        )
                    ), 0, binding.root.context.getString(R.string.duration).length, 0
                )
                durationTextSpan.setSpan(
                    StyleSpan(Typeface.BOLD),
                    0,
                    binding.root.context.getString(R.string.duration).length,
                    0
                )


                val duration = convertTimeToHumanReadable(tour.tourDuration.toString())+""
                val durationSpan = SpannableStringBuilder(duration)
                durationSpan.setSpan(
                    ForegroundColorSpan(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.colorPrimary
                        )
                    ), 0, duration.length, 0
                )
                durationSpan.setSpan(StyleSpan(Typeface.NORMAL), 0, duration.length, 0)

                tvDuration.text =  durationTextSpan.append(" ").append(durationSpan)

                tvTourName.text = tour.tourName
                tvDescription.text = tour.tourInfo
                tvPrice.text = "$"+tour.tourPrice
                if (tour.averageRating!=null){
                    ratingBar.rating = tour.averageRating?.toFloat()!!
                }

                tvMoreInfo.setOnClickListener {
                    onMoreInfoClick.invoke(tour)
                }

                tvBookNow.setOnClickListener {
                    onBookClick.invoke(tour)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ListItemBookingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(listOfTours[position])
    }

    override fun getItemCount(): Int {
        return listOfTours.size
    }

    fun convertTimeToHumanReadable(time: String): String {
        val parts = time.split(":")
        if (parts.size != 3) {
            return "Invalid time format"
        }

        val hours = parts[0].toInt()
        val minutes = parts[1].toInt()
        val seconds = parts[2].toInt()

        val totalMinutes = hours * 60 + minutes

        return when {
            totalMinutes >= 60 -> {
                val hourString = if (hours == 1) {
                    "1 hour"
                } else {
                    "$hours hours"
                }
                val minuteString = if (minutes > 0) {
                    if (minutes == 1) {
                        "1 minute"
                    } else {
                        "$minutes minutes"
                    }
                } else {
                    ""
                }
                "$hourString $minuteString"
            }
            totalMinutes > 0 -> {
                if (totalMinutes == 1) {
                    "$totalMinutes minute"
                } else {
                    "$totalMinutes minutes"
                }
            }
            else -> {
                if (seconds == 1) {
                    "$seconds second"
                } else {
                    "$seconds seconds"
                }
            }
        }
    }

}
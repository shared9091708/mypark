package com.divy.mypark.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.api.responsemodel.ParkSubCat
import com.divy.mypark.databinding.ListItemSelectWhatToDoBinding
import com.bumptech.glide.Glide

class ParkSubCategoryListAdapter(
    private val click : (position:Int) -> Unit,
    private val listOfParks: ArrayList<ParkSubCat>) :
    RecyclerView.Adapter<ParkSubCategoryListAdapter.ViewHolder>(){

    inner class ViewHolder(private val binding: ListItemSelectWhatToDoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(subCat: ParkSubCat) {
            binding.apply {
                Glide.with(binding.root.context).load(subCat.subCatofPark?.subCatImg).into(binding.ivImage)
                tvActivity.text = subCat.subCatofPark?.name
                mainCard.setOnClickListener {
                    subCat.subCategoryId?.let { it1 -> click.invoke(absoluteAdapterPosition) }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ListItemSelectWhatToDoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(listOfParks[position])
    }

    override fun getItemCount(): Int {
        return listOfParks.size
    }
}
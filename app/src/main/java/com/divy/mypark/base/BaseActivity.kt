package com.divy.mypark.base

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.divy.mypark.R
import com.divy.mypark.api.ApiResult
import com.divy.mypark.custom.showToast
import com.divy.mypark.listeners.PermissionListener
import com.divy.mypark.listeners.SnackbarListener
import com.divy.mypark.user.UserStateManager
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
open class BaseActivity : AppCompatActivity() {
    private var progressDialog: Dialog? = null

    @Inject
    lateinit var userStateManagerBase: UserStateManager

    /*internal var cameraPermissions =
        arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)*/

    fun showProgressDialog() {
        if (progressDialog == null || !progressDialog?.isShowing!!) {
            progressDialog = Dialog(this)
            progressDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            progressDialog?.setContentView(R.layout.custom_progressbar)
            progressDialog?.setCancelable(false)
            progressDialog?.show()
        }
    }

    fun hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog?.isShowing!!) {
                progressDialog?.dismiss()
            }
        } catch (throwable: Throwable) {

        } finally {
            progressDialog?.dismiss()
        }
    }

    fun <T> manageApiResult(
        apiResult: ApiResult<T>,
        showLoadingIndicator: Boolean = true,
        failureListener: ((String) -> Unit)? = null,
        successListener: (T, String) -> Unit,
    ) {
        when (apiResult) {
            ApiResult.Loading -> {
                if (showLoadingIndicator) {
                    showProgressDialog()
                }
            }
            is ApiResult.Success -> {
                if (showLoadingIndicator) {
                    hideProgressDialog()
                }
                successListener.invoke(apiResult.response, apiResult.message)
            }
            is ApiResult.Failure -> {
                hideProgressDialog()
                if (failureListener != null && !apiResult.isException) {
                    failureListener.invoke(apiResult.errorMessage)
                } else {
                    showToast(apiResult.errorMessage)
//                    CustomAlertDialog.showAlert(
//                        this,
//                        "",
//                        apiResult.errorMessage
//                    )
                }
            }
            ApiResult.NoInternet -> {
                hideProgressDialog()
                showToast(R.string.no_network_available)
            }
            ApiResult.SessionExpired -> {
                hideProgressDialog()
                userStateManagerBase.logout(this)
            }
            else -> {}
        }
    }

    fun showPermissionAlert(message: String, listener: PermissionListener) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.need_permission))
        builder.setMessage(message)
        builder.setPositiveButton(
            getString(R.string.ok),
            { dialog, which -> listener.onPermissionClick() })
        builder.setNeutralButton(getString(R.string.cancel), null)
        val dialog = builder.create()
        dialog.show()
    }

    protected fun showSnackbar(
        view: View,
        message: String,
        time: Int,
        snackbarListener: SnackbarListener,
    ) {
        val snackbar = Snackbar.make(view, message, time)
            .setAction(getString(R.string.go_to_settings), View.OnClickListener {
                snackbarListener.onSnackbarClick()
            })
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        snackbar.show()
    }
}
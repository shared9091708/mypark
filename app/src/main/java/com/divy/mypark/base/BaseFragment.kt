package com.divy.mypark.base

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.fragment.app.Fragment
import com.divy.mypark.R
import com.divy.mypark.api.ApiResult
import com.divy.mypark.custom.showToast
import com.divy.mypark.user.UserStateManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
open class BaseFragment : Fragment() {

    private var progressDialog: Dialog? = null

    @Inject
    lateinit var userStateManagerBase: UserStateManager

    fun showProgressDialog() {
        if (progressDialog == null || !progressDialog?.isShowing!!) {
            progressDialog = Dialog(requireContext())
            progressDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            progressDialog?.setContentView(R.layout.custom_progressbar)
            progressDialog?.setCancelable(false)
            progressDialog?.show()
        }
    }

    fun hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog?.isShowing!!) {
                progressDialog?.dismiss()
            }
        } catch (throwable: Throwable) {

        } finally {
            progressDialog?.dismiss()
        }
    }

    fun <T> manageApiResult(
        apiResult: ApiResult<T>,
        showLoadingIndicator: Boolean = true,
        failureListener: ((String) -> Unit)? = null,
        successListener: (T, String) -> Unit,
    ) {
        when (apiResult) {
            ApiResult.Loading -> {
                if (showLoadingIndicator) {
                    showProgressDialog()
                }
            }
            is ApiResult.Success -> {
                if (showLoadingIndicator) {
                    hideProgressDialog()
                }
                successListener.invoke(apiResult.response, apiResult.message)
            }
            is ApiResult.Failure -> {
                hideProgressDialog()
                if (failureListener != null && !apiResult.isException) {
                    failureListener.invoke(apiResult.errorMessage)
                } else {
//                    CustomAlertDialog.showAlert(
//                        this,
//                        "",
//                        apiResult.errorMessage
//                    )
                    showToast(apiResult.errorMessage)
                }
            }
            ApiResult.NoInternet -> {
                hideProgressDialog()
                showToast(R.string.no_network_available)
            }
            ApiResult.SessionExpired -> {
                hideProgressDialog()
                userStateManagerBase.logout(requireActivity())
            }
            else -> {}
        }
    }
}

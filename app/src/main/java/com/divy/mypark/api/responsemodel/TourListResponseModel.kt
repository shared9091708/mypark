package com.divy.mypark.api.responsemodel

import com.google.gson.annotations.SerializedName

data class TourListResponseModel(
    @SerializedName("count") var count: Int? = null,
    @SerializedName("tours") var tours: ArrayList<Tours> = arrayListOf()
)


data class TourGuide(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("profile_picture") var profilePicture: String? = null

)


data class Tours(

    @SerializedName("tour_id") var tourId: Int? = null,
    @SerializedName("park_id") var parkId: Int? = null,
    @SerializedName("tour_name") var tourName: String? = null,
    @SerializedName("tour_image") var tourImage: String? = null,
    @SerializedName("tour_guide") var tourGuide: TourGuide? = TourGuide(),
    @SerializedName("tour_price") var tourPrice: String? = null,
    @SerializedName("tour_info") var tourInfo: String? = null,
    @SerializedName("tour_duration") var tourDuration: String? = null,
    @SerializedName("max_people") var maxPeople: Int? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("deleted_at") var deletedAt: String? = null,
    @SerializedName("average_rating") var averageRating: String? = null

)
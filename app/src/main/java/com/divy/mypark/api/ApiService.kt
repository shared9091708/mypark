package com.divy.mypark.api

import com.divy.mypark.api.requestmodel.ChangePasswordRequestModel
import com.divy.mypark.api.requestmodel.ForgotPasswordRequestModel
import com.divy.mypark.api.requestmodel.LoginRequestModel
import com.divy.mypark.api.requestmodel.ObjectDetailRequestModel
import com.divy.mypark.api.requestmodel.ParkIdRequestModel
import com.divy.mypark.api.requestmodel.ParkListRequestModel
import com.divy.mypark.api.requestmodel.ParkObjectListRequestModel
import com.divy.mypark.api.requestmodel.RegisterRequestModel
import com.divy.mypark.api.requestmodel.TourListRequestModel
import com.divy.mypark.api.requestmodel.UpdateHuntRequestModel
import com.divy.mypark.api.responsemodel.AddSchavangerHuntResponseModel
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.api.responsemodel.ParkListResponseModel
import com.divy.mypark.api.responsemodel.ParkObjectListResponseModel
import com.divy.mypark.api.responsemodel.RegisterResponse
import com.divy.mypark.api.responsemodel.TourListResponseModel
import com.divy.mypark.api.responsemodel.UpdateHuntResponseModel
import com.divy.mypark.base.BaseRequestModel
import com.divy.mypark.base.BaseResponseModel
import com.divy.mypark.model.StateModel
import com.divy.mypark.model.StateRequestModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface ApiService {

    @POST(APIConstants.API_REGISTER)
    suspend fun callRegisterAPI(@Body requestBody: BaseRequestModel<RegisterRequestModel>): Response<BaseResponseModel<RegisterResponse>>

    @POST(APIConstants.API_SIGN_IN)
    suspend fun callLoginAPI(@Body requestBody: BaseRequestModel<LoginRequestModel>): Response<BaseResponseModel<LoginResponse>>

    @POST(APIConstants.API_FORGOT_PASSWORD)
    suspend fun callForgotPasswordAPI(@Body requestBody: BaseRequestModel<ForgotPasswordRequestModel>): Response<BaseResponseModel<Any>>

    @POST(APIConstants.API_LOGOUT)
    suspend fun callLogoutAPI(): Response<BaseResponseModel<Any>>

    @POST(APIConstants.API_CHANGE_PASSWORD)
    suspend fun callChangePasswordAPI(@Body requestBody: BaseRequestModel<ChangePasswordRequestModel>): Response<BaseResponseModel<Any>>

    @POST(APIConstants.API_GET_STATE)
    suspend fun getStateList(@Body requestBody: BaseRequestModel<StateRequestModel>): Response<BaseResponseModel<List<StateModel>>>

    @Multipart
    @POST(APIConstants.API_UPDATE_PROFILE)
    suspend fun updateProfile(@Part profile_photo: MultipartBody.Part?, @Part("data") data: RequestBody)
            : Response<BaseResponseModel<LoginResponse>>



    //parks
    @POST(APIConstants.PARK_LIST)
    suspend fun getParkList(@Body requestBody: BaseRequestModel<ParkListRequestModel>): Response<BaseResponseModel<ParkListResponseModel>>


    @POST(APIConstants.ADD_SCAVENGER_HUNT)
    suspend fun addScavengerHunt(@Body requestBody: BaseRequestModel<ParkIdRequestModel>): Response<BaseResponseModel<AddSchavangerHuntResponseModel>>


    @POST(APIConstants.PARK_OBJECT_LIST)
    suspend fun parkObjectList(@Body requestBody: BaseRequestModel<ParkObjectListRequestModel>): Response<BaseResponseModel<ParkObjectListResponseModel>>

    @POST(APIConstants.OBJECT_DETAIL)
    suspend fun objectDetail(@Body requestBody: BaseRequestModel<ObjectDetailRequestModel>): Response<BaseResponseModel<ObjectDetailResponseModel>>


    @POST(APIConstants.UPDATE_HUNT)
    suspend fun updateHunt(@Body requestBody: BaseRequestModel<UpdateHuntRequestModel>): Response<BaseResponseModel<UpdateHuntResponseModel>>

    @POST(APIConstants.TOUR_LIST)
    suspend fun tourList(@Body requestBody: BaseRequestModel<TourListRequestModel>): Response<BaseResponseModel<TourListResponseModel>>

}
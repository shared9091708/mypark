package com.divy.mypark.api.responsemodel

import com.google.gson.annotations.SerializedName

data class ParkObjectListResponseModel(
    @SerializedName("count") var count: Int? = null,
    @SerializedName("parkObjects") var parkObjects: ArrayList<ParkObjects> = arrayListOf()
)

data class ParkObjects(

    @SerializedName("park_id") var parkId: Int? = null,
    @SerializedName("object_image") var objectImage: String? = null,
    @SerializedName("object_file") var objectFile: String? = null,
    @SerializedName("object_sound") var objectSound: String? = null,
    @SerializedName("park_object_id") var parkObjectId: Int? = null,
    @SerializedName("object_name") var objectName: String? = null,
    @SerializedName("object_Indigenous_name") var objectIndigenousName: String? = null,
    @SerializedName("incorrect_ans") var incorrectAns: String? = null,
    @SerializedName("object_info") var objectInfo: String? = null,
    @SerializedName("count") var count: Int? = null

)

package com.divy.mypark.api.requestmodel

import com.google.gson.annotations.SerializedName

data class ParkIdRequestModel(
    @SerializedName("park_id") var parkId: String? = null
)

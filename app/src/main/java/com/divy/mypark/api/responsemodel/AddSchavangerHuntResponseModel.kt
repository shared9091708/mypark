package com.divy.mypark.api.responsemodel

import com.google.gson.annotations.SerializedName

data class AddSchavangerHuntResponseModel(
    @SerializedName("user_id") var userId: Int? = null,
    @SerializedName("park_id") var parkId: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("scavenger_hunt_id") var scavengerHuntId: Int? = null
)

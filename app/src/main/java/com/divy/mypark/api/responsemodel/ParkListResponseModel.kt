package com.divy.mypark.api.responsemodel

import com.google.gson.annotations.SerializedName


data class ParkListResponseModel(
    @SerializedName("count") var count: Int? = null,
    @SerializedName("parks") var parks: ArrayList<Parks> = arrayListOf()
)


data class SubCatofPark(

    @SerializedName("sub_category_id") var subCategoryId: Int? = null,
    @SerializedName("category_id") var categoryId: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("french_name") var frenchName: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("sub_cat_img") var subCatImg: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("deleted_at") var deletedAt: String? = null

)

data class ParkSubCat(

    @SerializedName("park_sub_cat_id") var parkSubCatId: Int? = null,
    @SerializedName("park_id") var parkId: Int? = null,
    @SerializedName("sub_category_id") var subCategoryId: Int? = null,
    @SerializedName("sub_catof_park") var subCatofPark: SubCatofPark? = SubCatofPark()

)


data class Parks(

    @SerializedName("distance") var distance: Double? = null,
    @SerializedName("park_id") var parkId: Int? = null,
    @SerializedName("park_name") var parkName: String? = null,
    @SerializedName("park_image") var parkImage: String? = null,
    @SerializedName("park_status") var parkStatus: String? = null,
    @SerializedName("park_lat") var parkLat: String? = null,
    @SerializedName("park_long") var parkLong: String? = null,
    @SerializedName("park_address") var parkAddress: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("deleted_at") var deletedAt: String? = null,
    @SerializedName("park_sub_cat") var parkSubCat: ArrayList<ParkSubCat> = arrayListOf()

)
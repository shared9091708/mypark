package com.divy.mypark.api.requestmodel

import com.google.gson.annotations.SerializedName

data class ParkListRequestModel(
    @SerializedName("page_no") var pageNo: String? = null,
    @SerializedName("offset") var offset: String? = null,
    @SerializedName("user_lat") var userLat: String? = null,
    @SerializedName("user_long") var userLong: String? = null
)

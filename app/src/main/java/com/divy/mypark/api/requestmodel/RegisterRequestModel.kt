package com.divy.mypark.api.requestmodel

import com.divy.mypark.api.APIConstants
import com.google.gson.annotations.SerializedName

data class RegisterRequestModel(
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("password") var password: String? = null,
    @SerializedName("confirm_password") var confirmPassword: String? = null,
    @SerializedName("device_type") var deviceType: String = APIConstants.DEVICE_TYPE,
)

package com.divy.mypark.api.requestmodel

import com.google.gson.annotations.SerializedName

data class UpdateHuntRequestModel(

    @SerializedName("scavenger_hunt_id") var scavengerHuntId: String? = null,
    @SerializedName("completed_time") var completedTime: String? = null,
    @SerializedName("park_object_id") var parkObjectId: String? = null,
    @SerializedName("count") var count: String? = null,
    @SerializedName("stop_hunt") var stopHunt: String? = null
)

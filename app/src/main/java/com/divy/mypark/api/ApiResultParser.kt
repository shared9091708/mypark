package com.divy.mypark.api

import com.divy.mypark.base.BaseResponseModel
import com.divy.mypark.utils.Utils
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response

inline fun <reified T : Any> safeApiCall(crossinline apiMethod: suspend () -> Response<BaseResponseModel<T>>,
): Flow<ApiResult<T>> {
    return flow {
        emit(ApiResult.Loading)
        if (Utils.isNetworkAvailable()) {
            try {
                val apiResponse = apiMethod.invoke()
                val responseModel = apiResponse.body()

                if (apiResponse.isSuccessful && responseModel?.status == APIConstants.SUCCESS) {
                    if (responseModel.data == null) {
                        responseModel.setResponse("")
                    }
                    val type = object : TypeToken<T>() {}.type
                    emit(
                        ApiResult.Success(
                            responseModel.getResponseModel(type),
                            responseModel.message ?: ""
                        )
                    )
                } else if (apiResponse.isSuccessful && responseModel?.status == 2) {
                    emit(ApiResult.SessionExpired)
                } else {
                    val errorMessage = responseModel?.message
                    if (errorMessage.isNullOrEmpty()) {
                        emit(ApiResult.Failure("Something went wrong!", true))
                    } else {
                        emit(ApiResult.Failure(errorMessage))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emit(ApiResult.Failure(e.message ?: "Something went wrong!", true))
            }
        } else {
            emit(ApiResult.NoInternet)
        }

    }.flowOn(Dispatchers.IO)
}
package com.divy.mypark.api.requestmodel

import com.google.gson.annotations.SerializedName

data class ParkObjectListRequestModel(
    @SerializedName("park_id") var parkId: String? = null,
    @SerializedName("page_no") var pageNo: String? = null,
    @SerializedName("offset") var offset: String? = null,
    @SerializedName("scavenger_hunt_id") var scavengerHuntId: String? = null
)

package com.divy.mypark.api.requestmodel

import com.google.gson.annotations.SerializedName

data class ObjectDetailRequestModel(
    @SerializedName("park_object_id" ) var parkObjectId : String? = null

)

package com.divy.mypark.api.responsemodel

import com.google.gson.annotations.SerializedName

data class RegisterResponse(
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("social_provider") var socialProvider: String? = null,
    @SerializedName("confirmation_code") var confirmationCode: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("id") var id: Int? = null,
    @SerializedName("roles") var roles: ArrayList<Roles> = arrayListOf(),
)

data class Roles(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("display_name") var displayName: String? = null,
    @SerializedName("guard_name") var guardName: String? = null,
    @SerializedName("auth_id") var authId: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("deleted_at") var deletedAt: String? = null,
    @SerializedName("pivot") var pivot: Pivot? = Pivot(),

    )


data class Pivot(

    @SerializedName("model_id") var modelId: Int? = null,
    @SerializedName("role_id") var roleId: Int? = null,
    @SerializedName("model_type") var modelType: String? = null,

    )
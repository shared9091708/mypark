package com.divy.mypark.api.responsemodel

import com.google.gson.annotations.SerializedName

data class UpdateHuntResponseModel(
    @SerializedName("scavenger_hunt_id") var scavengerHuntId: Int? = null,
    @SerializedName("user_id") var userId: Int? = null,
    @SerializedName("park_id") var parkId: Int? = null,
    @SerializedName("completed_time") var completedTime: String? = null,
    @SerializedName("total_hunts") var totalHunts: Int? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("deleted_at") var deletedAt: String? = null,
    @SerializedName("get_scavenger_hunt_user") var getScavengerHuntUser: ArrayList<GetScavengerHuntUser> = arrayListOf(),
    @SerializedName("get_scavenger_hunt_park") var getScavengerHuntPark: ArrayList<GetScavengerHuntPark> = arrayListOf(),
    @SerializedName("get_scavenger_hunt_detail") var getScavengerHuntDetail: ArrayList<GetScavengerHuntDetail> = arrayListOf()

)

data class GetScavengerHuntUser(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("first_name") var firstName: String? = null

)


data class GetScavengerHuntPark(

    @SerializedName("park_id") var parkId: Int? = null,
    @SerializedName("park_name") var parkName: String? = null,
    @SerializedName("park_image") var parkImage: String? = null,
    @SerializedName("park_status") var parkStatus: String? = null,
    @SerializedName("park_lat") var parkLat: String? = null,
    @SerializedName("park_long") var parkLong: String? = null,
    @SerializedName("park_address") var parkAddress: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("deleted_at") var deletedAt: String? = null

)


data class GetScavengerHuntDetail(

    @SerializedName("scavenger_hunt_detail_id") var scavengerHuntDetailId: Int? = null,
    @SerializedName("scavenger_hunt_id") var scavengerHuntId: Int? = null,
    @SerializedName("park_object_id") var parkObjectId: Int? = null,
    @SerializedName("count") var count: Int? = null

)
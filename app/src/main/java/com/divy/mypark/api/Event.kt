package com.divy.mypark.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.map

open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

fun <T> LiveData<T>.toSingleEvent(): LiveData<Event<T>> {
    return map {
        Event(it)
    }
}
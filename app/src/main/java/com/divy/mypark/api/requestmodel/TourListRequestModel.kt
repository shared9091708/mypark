package com.divy.mypark.api.requestmodel

import com.google.gson.annotations.SerializedName

data class TourListRequestModel(
    @SerializedName("park_id") var parkId: String? = null,
    @SerializedName("page_no") var pageNo: String? = null,
    @SerializedName("offset") var offset: String? = null
)

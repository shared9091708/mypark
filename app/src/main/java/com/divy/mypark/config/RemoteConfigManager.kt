package com.divy.mypark.config

interface RemoteConfigManager {
    fun checkRemoteConfig()
    fun getLatestAppVersion(): String
}
package com.divy.mypark.config

import android.content.Context
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.hilt.android.qualifiers.ApplicationContext
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject

class RemoteConfigManagerImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val firebaseRemoteConfig: FirebaseRemoteConfig,
) : RemoteConfigManager {
    override fun checkRemoteConfig() {
        firebaseRemoteConfig.fetchAndActivate().addOnSuccessListener {
            Timber.d("Values activated")
            EventBus.getDefault().post(RemoteConfigChanged())
        }.addOnFailureListener { e ->
            Timber.e(e)
        }
    }

    override fun getLatestAppVersion(): String {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        var version = remoteConfig.getString(RemoteConfigKey.LATEST_APP_VERSION)
        if (version.isBlank()) {
            version = context.let {
                it.packageManager.getPackageInfo(it.packageName, 0).versionName
            }
        }
        return version
    }
}
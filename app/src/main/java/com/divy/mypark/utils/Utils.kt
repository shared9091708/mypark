package com.divy.mypark.utils

import android.Manifest
import android.app.Activity
import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo
import android.content.ContentUris
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.startIntentSenderForResult
import androidx.core.content.ContextCompat
import com.divy.mypark.base.BaseApplication
import com.divy.mypark.location.LocationUtility
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.Priority
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.util.concurrent.TimeUnit


class Utils {

    companion object {
        private var gson: Gson? = null

        fun pxToDp(context: Context, px: Float): Float {
            val density = context.resources.displayMetrics.density
            return px / density
        }

        fun createLocationRequest(activity: Activity) {
            Log.e("<Location>", "createLocationRequest: ")
            val mLocationRequest = LocationRequest
                .Builder(Priority.PRIORITY_HIGH_ACCURACY, TimeUnit.SECONDS.toMillis(1)).
                apply {
                    setIntervalMillis(TimeUnit.SECONDS.toMillis(1))
                    setPriority(Priority.PRIORITY_HIGH_ACCURACY)
                    setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
//                setMinUpdateDistanceMeters(100F)
                    setWaitForAccurateLocation(true)
                }.build()
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
            val client = LocationServices.getSettingsClient(activity)
            val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
            task.addOnSuccessListener(activity
            ) {
                Log.e("<Location>", "createLocationRequest: addOnSuccessListener")
//            showToast("Enabled")
            }
            task.addOnFailureListener(activity) { e ->
                if (e is ResolvableApiException) {
                    Log.e("<Location>", "createLocationRequest: addOnFailureListener if")

                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        Log.e("<Location>", "createLocationRequest: addOnFailureListener if try")

//                        e.startResolutionForResult(
//                            activity,
//                            Constants.REQUEST_CHECK_SETTINGS_2
//                        )

                        startIntentSenderForResult(activity, e.resolution.intentSender,  Constants.REQUEST_CHECK_SETTINGS_2, null, 0, 0, 0,null)
                    } catch (sendEx: IntentSender.SendIntentException) {
                        Log.e("<Location>", "createLocationRequest: addOnFailureListener if catch")

                        // Ignore the error.
                    }
                }
            }
        }

        fun isGPSOn(context: Context): Boolean {
            val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

        fun requestMapPermissions(activity: Activity) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                LocationUtility.REQUEST_CHECK_SETTINGS
            )
        }


        fun createDrawableFromView(context: Context, view: View): Bitmap? {
            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay
                .getMetrics(displayMetrics)
            view.setLayoutParams(
                RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
            )
            view.measure(100, 100)
            view.layout(
                0, 0, 100,
                100
            )
            view.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(
                200,
                200, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            view.draw(canvas)
            return bitmap
        }

        fun isAppOnForeground(context: Context): Boolean {
            val activityManager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val appProcesses = activityManager.runningAppProcesses ?: return false
            for (appProcess in appProcesses) {
                if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName == context.packageName) {
                    return true
                }
            }
            return false
        }

        fun getGSONWithExpose(): Gson? {
            if (gson == null)
                gson = GsonBuilder().create()
            return gson
        }

        fun isNetworkAvailable(): Boolean {
            val cm =
                BaseApplication.getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return (netInfo != null && netInfo.isConnectedOrConnecting
                    && cm.activeNetworkInfo?.isAvailable == true
                    && cm.activeNetworkInfo?.isConnected == true)
        }

        fun getRequestBody(requestString: String): RequestBody {
            return requestString.toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
        }

        fun getFilePath(context: Context, uriFile: Uri): String? {
            var uri = uriFile
            var selection: String? = null
            var selectionArgs: Array<String>? = null
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split =
                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    if ("image" == type) {
                        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
            if ("content".equals(uri.scheme!!, ignoreCase = true)) {

                if (isGooglePhotosUri(uri)) {
                    return uri.lastPathSegment
                }

                val projection = arrayOf(MediaStore.Images.Media.DATA)
                var cursor: Cursor? = null
                try {
                    cursor = context.contentResolver
                        .query(uri, projection, selection, selectionArgs, null)
                    val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                    if (cursor.moveToFirst()) {
                        if (cursor.getString(column_index) != null)
                            return cursor.getString(column_index)
                        else return ""
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                }

            } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
                return uri.path
            }
            return null
        }

        private fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        private fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents".equals(uri.getAuthority());
        }

        private fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents".equals(uri.getAuthority());
        }

        private fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content".equals(uri.getAuthority());
        }

        fun isCameraAndGalleryPermissionGranted(context: Context): Boolean {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            )
                return true
            return false
        }

        @JvmStatic
        fun getAppVersionName(): String {
            var versionName = ""
            try {
                versionName = BaseApplication.getApplicationContext().packageManager
                    .getPackageInfo(
                        BaseApplication
                            .getApplicationContext().packageName, 0
                    ).versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            return versionName
        }
    }

}
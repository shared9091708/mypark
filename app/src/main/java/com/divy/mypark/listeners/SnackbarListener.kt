package com.divy.mypark.listeners

interface SnackbarListener {
    fun onSnackbarClick()
}
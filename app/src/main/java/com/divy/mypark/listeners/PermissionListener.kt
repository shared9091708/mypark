package com.divy.mypark.listeners

interface PermissionListener {
    fun onPermissionClick()
}
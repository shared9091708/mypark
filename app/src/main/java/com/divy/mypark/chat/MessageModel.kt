package com.divy.mypark.chat

import com.google.gson.annotations.SerializedName

data class MessageModel(
    @field:SerializedName("fromUserId",alternate = ["toUserId"] )
    val fromUserId: String = "",
    var imageURL: String = "",
    val name: String = "",
    val text: String = "",
    val timestamp: Long = 0L,
    val videoURL: String = "",
    val mediaType: String = "",
    val profileImageURL: String = ""
)
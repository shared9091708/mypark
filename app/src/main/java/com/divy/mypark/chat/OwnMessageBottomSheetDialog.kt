package com.divy.mypark.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.divy.mypark.R
import com.divy.mypark.databinding.BottomSheetOwnMessageBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class OwnMessageBottomSheetDialog(
    private val messageModel: MessageModel,
    private val key : String,
    private val position: Int,
    private val onEditClick: (messageModel:MessageModel,key:String,position:Int) -> Unit,
    private val onCopyClick: (messageModel:MessageModel,key:String) -> Unit,
    private val onDeleteClick: (messageModel:MessageModel,key:String,position:Int) -> Unit,
) : BottomSheetDialogFragment(), View.OnClickListener {


    private lateinit var binding: BottomSheetOwnMessageBinding

    fun newInstance(): OwnMessageBottomSheetDialog {
        return OwnMessageBottomSheetDialog(messageModel, key,position, onEditClick, onCopyClick, onDeleteClick)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_own_message, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.clickListener = this
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.linearEditMessage -> {
                onEditClick(messageModel, key,position)
                dismiss()
            }
            binding.linearCopyMessage -> {
                onCopyClick(messageModel, key)
                dismiss()
            }
            binding.linearDeleteMessage -> {
                onDeleteClick(messageModel, key,position)
                dismiss()
            }
        }
    }
}
package com.divy.mypark.chat

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.R
import com.divy.mypark.base.BaseApplication
import com.divy.mypark.databinding.ItemMessageBinding
import com.divy.mypark.utils.Constants
import com.divy.mypark.utils.DateTimeUtils
import com.bumptech.glide.Glide
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter

class ChatRvAdapter(
    private val mContext: Context,
    private val messageModelList: ArrayList<MessageModel>,
    private val opponentUserID: String,
    private val keyArrayList: ArrayList<String>,
    private val onOwnMessageClick: (messageModel: MessageModel, key: String, position: Int) -> Unit,
    private val onCancelEditClick: (messageModel: MessageModel, key: String, position: Int) -> Unit,
    private val onLongClick: (keyList: ArrayList<String>) -> Unit,
) :
    RecyclerView.Adapter<ChatRvAdapter.ViewHolder>(),
    StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

    var editing = false
    var editingPosition = -1
    var messageSelectedList: ArrayList<String> = arrayListOf()

    inner class ViewHolder(private val binding: ItemMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @RequiresApi(Build.VERSION_CODES.M)
        fun bindData(message: MessageModel, key: String) {

            binding.outgoingMessageParentRL.setOnLongClickListener {
                if (!editing) {
                    if (!messageSelectedList.isNullOrEmpty()) {
                        val found = messageSelectedList.find { it ->
                            it == key
                        }
                        if (found == null) {
                            Log.e("onLongClick", "NotFound")
                            binding.outgoingMessageParentRL.setBackgroundColor(
                                BaseApplication.getApplicationContext().getColor(R.color.gray100)
                            )
                            messageSelectedList.add(key)
                        } else {
                            Log.e("onLongClick", "Found")
                            messageSelectedList.remove(found)
                            binding.outgoingMessageParentRL.setBackgroundColor(
                                BaseApplication.getApplicationContext().getColor(R.color.colorWhite)
                            )
                        }
                    } else {
                        binding.outgoingMessageParentRL.setBackgroundColor(
                            BaseApplication.getApplicationContext().getColor(R.color.gray100)
                        )
                        messageSelectedList.add(key)
                    }
                    onLongClick(messageSelectedList)
                }
                true
            }

            binding.outgoingMessageParentRL.setOnClickListener {
                if (messageSelectedList.isNullOrEmpty()) {
                    if (!editing) {
                        onOwnMessageClick(message, key, absoluteAdapterPosition)
                    }
                } else {
                    val found = messageSelectedList.find { it ->
                        it == key
                    }
                    if (found == null) {
                        Log.e("onLongClick", "Single Click NotFound")
                        binding.outgoingMessageParentRL.setBackgroundColor(
                            BaseApplication.getApplicationContext().getColor(R.color.gray100)
                        )
                        messageSelectedList.add(key)
                    } else {
                        Log.e("onLongClick", "Single Click  Found")
                        messageSelectedList.remove(found)
                        binding.outgoingMessageParentRL.setBackgroundColor(
                            BaseApplication.getApplicationContext().getColor(R.color.colorWhite)
                        )
                    }
                    onLongClick(messageSelectedList)
                }
            }


            if (!editing) {
                binding.outgoingMessageParentRL.setBackgroundColor(
                    BaseApplication.getApplicationContext().getColor(
                        R.color.colorWhite
                    )
                )
                binding.cancelEdit.visibility = View.GONE
            } else {
                if (editingPosition == absoluteAdapterPosition) {
                    binding.outgoingMessageParentRL.setBackgroundColor(
                        BaseApplication.getApplicationContext().getColor(R.color.gray100)
                    )
                    binding.cancelEdit.visibility = View.VISIBLE
                } else {
                    binding.outgoingMessageParentRL.setBackgroundColor(
                        BaseApplication.getApplicationContext().getColor(R.color.colorWhite)
                    )
                    binding.cancelEdit.visibility = View.GONE
                }
            }

            binding.cancelEdit.setOnClickListener {
                onCancelEditClick(message, key, absoluteAdapterPosition)
            }

            val fromUserId = message.fromUserId
            if (fromUserId == opponentUserID) {
                when (message.mediaType) {
                    Constants.MEDIA_TYPE_TEXT -> {
                        //                    holder.binding.parentContentOutgoingRL.setBackgroundResource(R.drawable.sender_new_bg)
                        binding.messageType = Constants.MEDIA_TYPE_TEXT_INT
                    }
                    Constants.MEDIA_TYPE_IMAGE -> {
                        binding.outgoingMessageParentRL.background = null
                        binding.messageType = Constants.MEDIA_TYPE_IMAGE_INT
                    }
                    Constants.MEDIA_TYPE_VIDEO -> {
                        binding.outgoingMessageParentRL.background = null
                        binding.messageType = Constants.MEDIA_TYPE_VIDEO_INT
                    }
                }
                binding.model = message
                if (!message.profileImageURL.isNullOrEmpty()) {
                    Glide.with(mContext).load(message.profileImageURL)
                        .placeholder(R.drawable.ic_profile_placeholder)
                        .into(binding.civSender)
                }
                binding.isIncomingMessage = false
            } else {
                when (message.mediaType) {
                    Constants.MEDIA_TYPE_TEXT -> {
                        //                    holder.binding.parentContentIncomingRL.setBackgroundResource(R.drawable.receiver_new_bg)
                        binding.messageType = Constants.MEDIA_TYPE_TEXT_INT
                    }
                    Constants.MEDIA_TYPE_IMAGE -> {
                        binding.incomingMessageParentRL.background = null
                        binding.messageType = Constants.MEDIA_TYPE_IMAGE_INT
                    }
                    Constants.MEDIA_TYPE_VIDEO -> {
                        binding.incomingMessageParentRL.background = null
                        binding.messageType = Constants.MEDIA_TYPE_VIDEO_INT
                    }
                }
                if (!message.profileImageURL.isNullOrEmpty()) {
                    Glide.with(mContext).load(message.profileImageURL)
                        .placeholder(R.drawable.ic_profile_placeholder).into(binding.civOppponent)
                }
                binding.model = message
                binding.isIncomingMessage = true
            }

        }
    }

    fun clearMessageSelectedList() {
        messageSelectedList.clear()
    }

    fun resetEditing(position: Int) {
        this.editing = false
        this.editingPosition = -1
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(messageModelList[position], key = keyArrayList[position])
    }

    override fun getItemCount(): Int {
        return messageModelList.size
    }

    override fun getHeaderId(position: Int): Long {
        return messageModelList[position].timestamp.let { DateTimeUtils.getDateAsHeaderId((it)) }
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup?): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.chat_message_date_header, parent, false)
        return object : RecyclerView.ViewHolder(view) {}
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val view = holder?.itemView
        val dateTextView = view!!.findViewById<TextView>(R.id.header_date_textview)
        dateTextView.setText(messageModelList[position].timestamp.let {
            DateTimeUtils.parseChatSectioDate(
                it
            )
        })
    }
}
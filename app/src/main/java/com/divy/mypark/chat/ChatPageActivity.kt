package com.divy.mypark.chat

import android.content.ClipData
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.ClipboardManager
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.custom.showKeyboard
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.ActivityChatPageBinding
import com.divy.mypark.utils.Constants
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import java.util.*
import kotlin.collections.ArrayList


class ChatPageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChatPageBinding
    private lateinit var databaseReference: DatabaseReference
    private lateinit var childDatabaseReference: DatabaseReference
    private var linearLayoutManager: LinearLayoutManager? = null
    private var messageAdapter: ChatRvAdapter? = null
    private var isMessageSent = false

    private var editMessage = false
    private var editKey: String = ""
    private var positionFromAdapter: Int = -1

    private var databaseChildName: String = ""
    private var keyArraylist: ArrayList<String> = arrayListOf()
    private var childEventListener: ChildEventListener? = null
    private var valueEventListener: ValueEventListener? = null
    private var messageModelList = arrayListOf<MessageModel>()
    private var keyListToDelete = arrayListOf<String>()
    private var countChat = 0L

    var chatId: String? = null
    var userId: String? = null
    var userName: String? = null

    var profileImageUrl: String? = null

    lateinit var ownMessageBottomSheetDialog: OwnMessageBottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =
            DataBindingUtil.setContentView(this, com.divy.mypark.R.layout.activity_chat_page)

        binding.lifecycleOwner = this
        binding.isChatAllowed = true

        chatId = intent.getStringExtra(Constants.CHAT_ID)
        userId = intent.getStringExtra(Constants.USER_ID)
        userName = intent.getStringExtra(Constants.USER_NAME)

        binding.toolbar.tvTitle.text = userName
        if (userId.equals("1")) {
            profileImageUrl =
                "https://i2.wp.com/beebom.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg"
        } else {
            profileImageUrl = "https://picsum.photos/id/237/200/200.jpg"
        }
        initView()
        clicks()
    }

    private fun clicks() {
        binding.sendButton.setOnClickListener {
            if (binding.messageInputET.text.toString().trim().isNotEmpty()) {
                if (editMessage && !editKey.isNullOrEmpty()) {
                    Log.e("TestingMessage", "onClick sendEditedMessage")
                    sendEditedMessage(editKey)
                } else {
                    Log.e("TestingMessage", "onClick sendChatMessage")
                    sendChatMessage()
                }
            }
        }
        binding.toolbar.ivBack.setOnClickListener {
            onBackPressed()
        }
        binding.toolbar.ivMedia.setOnClickListener {
            if (!keyListToDelete.isNullOrEmpty()) {
                keyListToDelete.forEach {
                    childDatabaseReference.child(it).removeValue { error, ref ->

                    }
                }
            }
        }
    }

    private fun sendChatMessage() {
        val message =
            MessageModel(
                fromUserId = userId.toString(),
                profileImageURL = profileImageUrl.toString(),
                imageURL = "",
                name = userName.toString(),
                text = binding.messageInputET.text.toString(),
                timestamp = Date().time,
                videoURL = "",
                mediaType = Constants.MEDIA_TYPE_TEXT
            )

        childDatabaseReference.push().setValue(message).addOnSuccessListener {
            Log.e("TestingMessage", "FIRST addOnSuccessListener")

        }.addOnCanceledListener {
            Log.e("TestingMessage", "FIRST addOnCanceledListener")

        }.addOnFailureListener {
            Log.e("TestingMessage", "FIRST addOnFailureListener")

        }
        binding.messageInputET.setText("")
        scrollToBottom()
        isMessageSent = true
    }

    private fun sendEditedMessage(key: String) {
        Log.e("TestingMessage", "Called sendEditedMessage")
        childDatabaseReference.child(key).child("text")
            .setValue(binding.messageInputET.text.toString()).addOnSuccessListener {
                Log.e("TestingMessage", "EDITED addOnSuccessListener")
            }.addOnCanceledListener {
                Log.e("TestingMessage", "EDITED addOnCanceledListener")

            }.addOnFailureListener {
                Log.e("TestingMessage", "EDITED addOnFailureListener")
            }
        binding.messageInputET.setText("")
        isMessageSent = true
        messageAdapter!!.resetEditing(positionFromAdapter)
        editMessage = false
        editKey = ""
    }

    private fun initView() {
        Firebase.database.goOnline()
        databaseChildName = com.divy.mypark.BuildConfig.CHAT_NODE
        connectToFirebaseDatabase()
    }

    private fun connectToFirebaseDatabase() {
        Log.e("TestingMessage", "Called connectToFirebaseDatabase")
        databaseReference = Firebase.database.reference
        childDatabaseReference =
            databaseReference.child(databaseChildName).child(chatId.toString())

        messageAdapter = ChatRvAdapter(this,
            messageModelList,
            userId.toString(),
            keyArraylist,
            ::ownMessageClick,
            ::onCancelEditClick,
            ::onLongClick)

        childEventListener =
            childDatabaseReference.addChildEventListener(object : ChildEventListener {
                override fun onCancelled(snapshot: DatabaseError) {

                }

                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                    Log.e("TestingMessage", "connectToFirebaseDatabase onChildMoved")
                }

                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                    try {
                        val messageModel = snapshot.getValue(MessageModel::class.java)
                        Log.e("TestingMessage", "connectToFirebaseDatabase onChildChanged ")
                        Log.e("TestingMessage", "Position after change $positionFromAdapter")
                        messageModelList.removeAt(positionFromAdapter)
                        messageModelList.add(positionFromAdapter, messageModel!!)
                        messageAdapter!!.notifyDataSetChanged()

                    } catch (e: java.lang.Exception) {
                        Log.e("TestingMessage", "onChildChanged Catch")
                    }
                }

                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    try {
                        Log.e("TestingMessage", "connectToFirebaseDatabase onChildAdded")

                        val messageModel = snapshot.getValue(MessageModel::class.java)
                        if (messageModel != null) {
                            messageModelList.add(messageModel)
                            keyArraylist.add(snapshot.key.toString())
                        }
                        messageAdapter?.notifyDataSetChanged()
                    } catch (e: java.lang.Exception) {
                        Log.e("TestingMessage", "onChildAdded Catch")
                    }
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                    try {
                        val messageModel = snapshot.getValue(MessageModel::class.java)
                        messageModelList.remove(messageModel)
                        keyArraylist.remove(snapshot.key)
                        messageAdapter!!.notifyDataSetChanged()

                        Log.e("TestingMessage", "connectToFirebaseDatabase onChildRemoved")
                    } catch (e: java.lang.Exception) {
                        Log.e("TestingMessage", "onChildRemoved Catch")
                    }
                    binding.toolbar.ivMedia.visibility = View.INVISIBLE
                    if (messageAdapter != null) {
                        messageAdapter!!.clearMessageSelectedList()
                    }
                }
            })

        valueEventListener =
            childDatabaseReference.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.e("TestingMessage", "addValueEventListener onDataChange")
                    countChat++
                    Handler(Looper.myLooper()!!).postDelayed({
                        scrollToBottom()
                    }, 500)
                }
            })

        linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.recChat.layoutManager = linearLayoutManager
        binding.recChat.adapter = messageAdapter
        binding.recChat.addItemDecoration(StickyRecyclerHeadersDecoration(messageAdapter))

    }

    private fun onLongClick(keyList: ArrayList<String>) {
        keyListToDelete = keyList
        if (keyList.isNullOrEmpty()) {
            Log.e("onLongClick", "null " + keyList.toString())
            binding.toolbar.ivMedia.visibility = View.INVISIBLE
        } else {
            Log.e("onLongClick", "not null" + keyList.toString())
            binding.toolbar.ivMedia.visibility = View.VISIBLE
            binding.toolbar.ivMedia.setImageResource(com.divy.mypark.R.drawable.ic_delete)
        }
    }

    private fun ownMessageClick(messageModel: MessageModel, key: String, position: Int) {
        ownMessageBottomSheetDialog =
            OwnMessageBottomSheetDialog(messageModel,
                key,
                position,
                ::onEditClick,
                ::onCopyClick,
                ::onDeleteClick).newInstance()
        ownMessageBottomSheetDialog.show(supportFragmentManager, "Own Msg")
    }

    private fun onCancelEditClick(messageModel: MessageModel, key: String, position: Int) {
        messageAdapter?.resetEditing(position)
        binding.messageInputET.setText("")
        editMessage = false
        editKey = ""
    }

    private fun onEditClick(messageModel: MessageModel, key: String, position: Int) {
        if (messageAdapter?.editing!!) {
            var oldEditPos = messageAdapter?.editingPosition
            messageAdapter?.notifyItemChanged(oldEditPos!!)
        }
        messageAdapter?.editing = true
        messageAdapter?.editingPosition = position
        messageAdapter?.notifyItemChanged(position)

        editKey = key
        editMessage = true
        positionFromAdapter = position
        Log.e("TestingMessage", "onEditClick Position " + position.toString())
        binding.messageInputET.setText(messageModel.text)
        binding.messageInputET.setSelection(binding.messageInputET.length())
        binding.messageInputET.requestFocus(1)
        showKeyboard(binding.messageInputET)
    }

    private fun onCopyClick(messageModel: MessageModel, key: String) {
        val sdk = Build.VERSION.SDK_INT
        if (sdk < Build.VERSION_CODES.HONEYCOMB) {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.text = messageModel.text
        } else {
            val clipboard =
                getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = ClipData.newPlainText("", messageModel.text)
            clipboard.setPrimaryClip(clip)
        }
        showToast("Message Copied")
    }

    private fun onDeleteClick(messageModel: MessageModel, key: String, position: Int) {
        positionFromAdapter = position
        childDatabaseReference.child(key).removeValue { error, ref ->
//            showToast("Message Deleted", 0)
        }
    }

    private fun scrollToBottom() {
        binding.recChat.scrollToPosition(messageModelList.size - 1)
    }
}
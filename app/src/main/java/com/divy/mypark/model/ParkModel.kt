package com.divy.mypark.model

data class ParkModel(
    var image: Int,
    var parkName: String,
)

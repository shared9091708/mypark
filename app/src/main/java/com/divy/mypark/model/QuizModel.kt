package com.divy.mypark.model

data class QuizModel(
    var answer: String,
    var color: Int,
    var isClickEnabled: Boolean,
)

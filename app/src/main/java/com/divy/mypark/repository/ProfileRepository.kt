package com.divy.mypark.repository

import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.model.StateModel
import com.divy.mypark.model.StateRequestModel
import com.divy.mypark.model.UpdateProfileRequestModel
import kotlinx.coroutines.flow.Flow

interface ProfileRepository {
    fun getStateList(stateRequestModel: StateRequestModel): Flow<ApiResult<List<StateModel>>>
    fun updateProfile(updateProfileRequestModel: UpdateProfileRequestModel): Flow<ApiResult<LoginResponse>>
}
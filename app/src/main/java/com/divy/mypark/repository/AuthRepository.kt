package com.divy.mypark.repository

import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.requestmodel.ChangePasswordRequestModel
import com.divy.mypark.api.requestmodel.ForgotPasswordRequestModel
import com.divy.mypark.api.requestmodel.LoginRequestModel
import com.divy.mypark.api.requestmodel.RegisterRequestModel
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.responsemodel.RegisterResponse
import kotlinx.coroutines.flow.Flow

interface AuthRepository {
    fun login(requestModel: LoginRequestModel): Flow<ApiResult<LoginResponse>>
    fun signUp(registerRequestModel: RegisterRequestModel): Flow<ApiResult<RegisterResponse>>
    fun forgotPassword(forgotPasswordRequestModel: ForgotPasswordRequestModel): Flow<ApiResult<Any>>
    fun changePassword(changePasswordRequestModel: ChangePasswordRequestModel): Flow<ApiResult<Any>>
    fun logout(): Flow<ApiResult<Any>>
}
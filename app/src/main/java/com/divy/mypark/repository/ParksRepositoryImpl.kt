package com.divy.mypark.repository

import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.ApiService
import com.divy.mypark.api.requestmodel.ObjectDetailRequestModel
import com.divy.mypark.api.requestmodel.ParkIdRequestModel
import com.divy.mypark.api.requestmodel.ParkListRequestModel
import com.divy.mypark.api.requestmodel.ParkObjectListRequestModel
import com.divy.mypark.api.requestmodel.TourListRequestModel
import com.divy.mypark.api.requestmodel.UpdateHuntRequestModel
import com.divy.mypark.api.responsemodel.AddSchavangerHuntResponseModel
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.api.responsemodel.ParkListResponseModel
import com.divy.mypark.api.responsemodel.ParkObjectListResponseModel
import com.divy.mypark.api.responsemodel.TourListResponseModel
import com.divy.mypark.api.responsemodel.UpdateHuntResponseModel
import com.divy.mypark.api.safeApiCall
import com.divy.mypark.base.BaseRequestModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ParksRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : ParksRepository {
    override fun getParkList(parkListRequestModel: ParkListRequestModel): Flow<ApiResult<ParkListResponseModel>> {
        return safeApiCall {
            apiService.getParkList(
                BaseRequestModel(parkListRequestModel)
            )
        }
    }

    override fun addScavengerHunt(parkIdRequestModel: ParkIdRequestModel): Flow<ApiResult<AddSchavangerHuntResponseModel>> {
        return safeApiCall {
            apiService.addScavengerHunt(BaseRequestModel(parkIdRequestModel))
        }
    }

    override fun parkObjectList(parkObjectListRequestModel: ParkObjectListRequestModel): Flow<ApiResult<ParkObjectListResponseModel>> {
        return safeApiCall {
            apiService.parkObjectList(BaseRequestModel(parkObjectListRequestModel))
        }
    }

    override fun objectDetail(objectDetailRequestModel: ObjectDetailRequestModel): Flow<ApiResult<ObjectDetailResponseModel>> {
        return safeApiCall {
            apiService.objectDetail(BaseRequestModel(objectDetailRequestModel))
        }
    }

    override fun updateHunt(updateHuntRequestModel: UpdateHuntRequestModel): Flow<ApiResult<UpdateHuntResponseModel>> {
        return safeApiCall {
            apiService.updateHunt(BaseRequestModel(updateHuntRequestModel))
        }
    }

    override fun tourList(tourListRequestModel: TourListRequestModel): Flow<ApiResult<TourListResponseModel>> {
        return safeApiCall {
            apiService.tourList(BaseRequestModel(tourListRequestModel))
        }
    }
}
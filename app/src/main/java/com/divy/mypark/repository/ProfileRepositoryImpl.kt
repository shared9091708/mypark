package com.divy.mypark.repository

import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.ApiService
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.safeApiCall
import com.divy.mypark.base.BaseRequestModel
import com.divy.mypark.model.StateModel
import com.divy.mypark.model.StateRequestModel
import com.divy.mypark.model.UpdateProfileRequestModel
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

class ProfileRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : ProfileRepository {
    override fun getStateList(stateRequestModel: StateRequestModel): Flow<ApiResult<List<StateModel>>> {
        return safeApiCall {
            apiService.getStateList(
                BaseRequestModel(stateRequestModel)
            )
        }
    }

    override fun updateProfile(updateProfileRequestModel: UpdateProfileRequestModel): Flow<ApiResult<LoginResponse>> {
        return safeApiCall {
            var part: MultipartBody.Part? = null
            val requestModel = BaseRequestModel(updateProfileRequestModel)
            if (!updateProfileRequestModel.profilePhoto.isNullOrEmpty()) {
                val file = File(updateProfileRequestModel.profilePhoto)
                // Create a request body with file and image media type
                val fileReqBody = file.asRequestBody("image/*".toMediaTypeOrNull())
                part = MultipartBody.Part.createFormData("profile_image", file.name, fileReqBody)
            }

            val dataRequestBody =
                Gson().toJson(requestModel)
                    .toRequestBody("multipart/form-data".toMediaTypeOrNull())

            apiService.updateProfile(
                part, dataRequestBody
            )
        }
    }


}
package com.divy.mypark.repository

import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.requestmodel.ObjectDetailRequestModel
import com.divy.mypark.api.requestmodel.ParkIdRequestModel
import com.divy.mypark.api.requestmodel.ParkListRequestModel
import com.divy.mypark.api.requestmodel.ParkObjectListRequestModel
import com.divy.mypark.api.requestmodel.TourListRequestModel
import com.divy.mypark.api.requestmodel.UpdateHuntRequestModel
import com.divy.mypark.api.responsemodel.AddSchavangerHuntResponseModel
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.api.responsemodel.ParkListResponseModel
import com.divy.mypark.api.responsemodel.ParkObjectListResponseModel
import com.divy.mypark.api.responsemodel.TourListResponseModel
import com.divy.mypark.api.responsemodel.UpdateHuntResponseModel
import kotlinx.coroutines.flow.Flow

interface ParksRepository {
    fun getParkList(parkListRequestModel: ParkListRequestModel): Flow<ApiResult<ParkListResponseModel>>
    fun addScavengerHunt(parkIdRequestModel: ParkIdRequestModel): Flow<ApiResult<AddSchavangerHuntResponseModel>>
    fun parkObjectList(parkObjectListRequestModel: ParkObjectListRequestModel): Flow<ApiResult<ParkObjectListResponseModel>>
    fun objectDetail(objectDetailRequestModel: ObjectDetailRequestModel): Flow<ApiResult<ObjectDetailResponseModel>>
    fun updateHunt(updateHuntRequestModel: UpdateHuntRequestModel): Flow<ApiResult<UpdateHuntResponseModel>>
    fun tourList(tourListRequestModel: TourListRequestModel): Flow<ApiResult<TourListResponseModel>>
}
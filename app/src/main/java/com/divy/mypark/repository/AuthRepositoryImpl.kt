package com.divy.mypark.repository

import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.ApiService
import com.divy.mypark.api.requestmodel.ChangePasswordRequestModel
import com.divy.mypark.api.requestmodel.ForgotPasswordRequestModel
import com.divy.mypark.api.requestmodel.LoginRequestModel
import com.divy.mypark.api.requestmodel.RegisterRequestModel
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.responsemodel.RegisterResponse
import com.divy.mypark.api.safeApiCall
import com.divy.mypark.base.BaseRequestModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : AuthRepository {

    override fun login(requestModel: LoginRequestModel): Flow<ApiResult<LoginResponse>> {
        return safeApiCall {
            apiService.callLoginAPI(
                BaseRequestModel(requestModel)
            )
        }
    }

    override fun signUp(registerRequestModel: RegisterRequestModel): Flow<ApiResult<RegisterResponse>> {
        return safeApiCall {
            apiService.callRegisterAPI(
                BaseRequestModel(registerRequestModel)
            )
        }
    }

    override fun forgotPassword(forgotPasswordRequestModel: ForgotPasswordRequestModel): Flow<ApiResult<Any>> {
        return safeApiCall {
            apiService.callForgotPasswordAPI(
                BaseRequestModel(forgotPasswordRequestModel)
            )
        }
    }

    override fun changePassword(changePasswordRequestModel: ChangePasswordRequestModel): Flow<ApiResult<Any>> {
        return safeApiCall {
            apiService.callChangePasswordAPI(
                BaseRequestModel(changePasswordRequestModel)
            )
        }
    }

    override fun logout(): Flow<ApiResult<Any>> {
        return safeApiCall {
            apiService.callLogoutAPI()
        }
    }


}
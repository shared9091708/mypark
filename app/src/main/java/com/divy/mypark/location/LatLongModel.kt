package com.divy.mypark.location

import android.os.Parcel
import android.os.Parcelable

data class LatLongModel (
    var latitude: Double,
    var longitude: Double
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LatLongModel> {
        override fun createFromParcel(parcel: Parcel): LatLongModel {
            return LatLongModel(parcel)
        }

        override fun newArray(size: Int): Array<LatLongModel?> {
            return arrayOfNulls(size)
        }
    }
}
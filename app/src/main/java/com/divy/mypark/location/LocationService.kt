package com.divy.mypark.location

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.divy.mypark.R
import com.divy.mypark.ui.activities.LiveLocationActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class LocationService : Service() {

    private val CHANNEL_ID = "LocationService"
    var latitude = 0.0
    var longitude = 0.0
    private var firebaseReference: DatabaseReference? = null
    private val LIVE_LOCATION_DATA = "LiveLocationData"

    override fun onCreate() {
        super.onCreate()

        firebaseReference = FirebaseDatabase.getInstance().reference
        //Function to show notification with latitude and longitude of user
        showNotification(latitude, longitude)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        //Function to get current location using LocationManager Api
        getCurrentLocation(this)

        //Function to show live location on map
//        startMapActivity()

        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
    private fun getCurrentLocation(context: Context?) {
        Log.e("FusedLocation", "Called getCurrentLocation")
        //Check all location permission granted or not
        if (ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

            //Criteria class indicating the application criteria for selecting a location provider
            val criteria = Criteria()
            criteria.accuracy = Criteria.ACCURACY_FINE
            criteria.isSpeedRequired = true

            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val provider = locationManager.getBestProvider(criteria, true)

            if (provider != null) {
                locationManager.requestLocationUpdates(
                    provider, 1, 0.1f, object : LocationListener {
                        override fun onLocationChanged(location: Location) {
                            //Location changed
                            latitude = location.latitude
                            longitude = location.longitude
                            Log.e("FusedLocation", "Location Changed "+location.latitude.toString()+" "+location.longitude.toString())
                            shareLocationToFireBase("1",location.latitude.toString(),location.longitude.toString())
                            showNotification(latitude, longitude)
                        }

                        override fun onProviderDisabled(provider: String) {
                            //Provider disabled
                        }

                        override fun onProviderEnabled(provider: String) {
                            //Provider enabled
                        }

                        override fun onStatusChanged(
                            provider: String?,
                            status: Int,
                            extras: Bundle?,
                        ) {

                        }

                    })
            }
        }
    }

    private fun showNotification(latitude: Double, longitude: Double) {

        //It requires to create notification channel when android version is more than or equal to oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }

        val notificationIntent = Intent(this, LiveLocationActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Location Service")
            .setContentText("Latitude = $latitude Longitude = $longitude")
            .setSmallIcon(R.drawable.ic_location)
            .setContentIntent(pendingIntent)
            .setOngoing(true)
            .build()
        startForeground(1, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val serviceChannel = NotificationChannel(
            CHANNEL_ID, "Location Service Channel",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        val manager = getSystemService(NotificationManager::class.java)
        manager!!.createNotificationChannel(serviceChannel)

    }

    private fun shareLocationToFireBase(
        livelocationid: String,
        latitude: String,
        longitude: String,
    ) {
//        live loaction id for particular tracking
        var livelocData = LatLongModel(latitude.toDouble(), longitude.toDouble())

//        pass user id in child
//        var childid = UserStateManager.getUserProfile()!!.userId.toDouble().toInt().toString()

        firebaseReference!!.child(LIVE_LOCATION_DATA)
            .child(livelocationid.toString())
            .child("10")
            .setValue(livelocData)
    }
}
package com.divy.mypark.location

import android.Manifest
import android.app.*
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.divy.mypark.ui.activities.LiveLocationActivity
import com.google.android.gms.location.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class BackgroundService : Service() {

    var counter = 0
    private var firebaseReference: DatabaseReference? = null

    private val LIVE_LOCATION_DATA = "LiveLocationData"
    private val CHANNEL_ID = "LocationService"
    private var mLocationRequest: LocationRequest? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)

            val latitude = locationResult.locations[0]?.latitude
            val longitude = locationResult.locations[0]?.longitude

//            shareLocationToFireBase("1", latitude.toString(), longitude.toString())

            Log.e("FusedLocation",
                "Location Result " + latitude.toString() + " " + longitude.toString())
        }
    }

    private fun initLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = LocationUtility.UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest?.fastestInterval = LocationUtility.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun getLocation() {

        Log.e("FusedLocation", "Called getLocation")

        mLocationRequest?.let {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            fusedLocationProviderClient?.requestLocationUpdates(
                it,
                locationCallback,
                Looper.getMainLooper()
            )
        }
    }

    override fun onCreate() {
        super.onCreate()

        firebaseReference = FirebaseDatabase.getInstance().reference
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            startMyOwnForeground()
        } else {
            startForeground(
                1,
                Notification())
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun startMyOwnForeground() {
//        val NOTIFICATION_CHANNEL_ID = "example.permanence"
//        val channelName = "Background Service"
//        val chan = NotificationChannel(NOTIFICATION_CHANNEL_ID,
//            channelName,
//            NotificationManager.IMPORTANCE_NONE)
//        chan.lightColor = Color.BLUE
//        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
//        val manager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
//        manager.createNotificationChannel(chan)
//        val notificationBuilder: NotificationCompat.Builder =
//            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
//        val notification: Notification = notificationBuilder.setOngoing(true)
//            .setContentTitle("App is running in background")
//            .setPriority(NotificationManager.IMPORTANCE_MIN)
//            .setCategory(Notification.CATEGORY_SERVICE)
//            .build()
//        startForeground(2, notification)

        createNotificationChannel()
        showNotification()
    }

    private fun showNotification() {

        //It requires to create notification channel when android version is more than or equal to oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }

        val notificationIntent = Intent(this, LiveLocationActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Location Service")
            .setContentText("Background Location Access Service Is Running")
            .setSmallIcon(com.divy.mypark.R.drawable.ic_location)
            .setContentIntent(pendingIntent)
            .setOngoing(true)
            .build()
        startForeground(1, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val serviceChannel = NotificationChannel(
            CHANNEL_ID, "Location Service Channel",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        val manager = getSystemService(NotificationManager::class.java)
        manager!!.createNotificationChannel(serviceChannel)

    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        initLocationRequest()
        getLocation()
//        startTimer()
        Log.e("ServiceTesting", "onStartCommand")
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
//        stoptimertask()

        //stopgetlocation

        val broadcastIntent = Intent()
        broadcastIntent.action = "restartservice"
        broadcastIntent.setClass(this, ServiceRestarter::class.java)
        this.sendBroadcast(broadcastIntent)
    }


//    private var timer: Timer? = null
//    private var timerTask: TimerTask? = null
//    fun startTimer() {
//        timer = Timer()
//        timerTask = object : TimerTask() {
//            override fun run() {
//
//                initLocationRequest()
//                getLocation()
////                initLocationService()
//                //                Toast.makeText(this@BackgroundService,"Service "+counter++,Toast.LENGTH_SHORT).show()
//                Log.i("Count", "=========  " + counter++)
//            }
//        }
//        timer!!.schedule(timerTask, 2000, 1000) //
//    }
//
//    fun stoptimertask() {
//        if (timer != null) {
//            timer!!.cancel()
//            timer = null
//        }
//    }

    @Nullable
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun shareLocationToFireBase(
        livelocationid: String,
        latitude: String,
        longitude: String,
    ) {
//        live loaction id for particular tracking
        var livelocData = LatLongModel(latitude.toDouble(), longitude.toDouble())

//        pass user id in child
//        var childid = UserStateManager.getUserProfile()!!.userId.toDouble().toInt().toString()

        firebaseReference!!.child(LIVE_LOCATION_DATA)
            .child(livelocationid.toString())
            .child("10")
            .setValue(livelocData)
    }
}
package com.divy.mypark.user

import android.app.Activity
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.api.responsemodel.Parks

interface UserStateManager {
    fun isOnBoardingCompleted(): Boolean
    fun markOnBoardingAsComplete()
    fun isUserLoggedIn(): Boolean
    fun getFirebaseToken(): String
    fun saveFirebaseToken(token: String)
    fun getAuthToken(): String
    fun saveAuthToken(token: String)
    fun getUserProfile(): LoginResponse?
    fun saveUserProfile(loginResponse: LoginResponse)


    fun getObjectDetails(): ObjectDetailResponseModel?
    fun saveObjectDetails(objectDetailResponseModel: ObjectDetailResponseModel)
    fun saveHuntId(id:Int)

    fun getHuntId():Int

    fun logout(activity: Activity)


    fun saveSelectedPark(parks: Parks)

    fun getSelectedPark(): Parks?
}
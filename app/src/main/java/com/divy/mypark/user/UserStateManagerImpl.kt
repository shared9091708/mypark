package com.divy.mypark.user

import android.app.Activity
import android.text.TextUtils
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.preference.PreferenceHelper
import com.divy.mypark.ui.activities.LoginContainerActivity
import com.divy.mypark.utils.Constants
import com.google.gson.Gson
import javax.inject.Inject

class UserStateManagerImpl @Inject constructor(
    private val preferenceHelper: PreferenceHelper,
    private val gson: Gson,
) : UserStateManager {

    override fun markOnBoardingAsComplete() {
        preferenceHelper.setValue(Constants.PREF_IS_INTRODUCTION_FINISHED_BOOL, true)
    }

    override fun isOnBoardingCompleted(): Boolean {
        return preferenceHelper.getValue(
            Constants.PREF_IS_INTRODUCTION_FINISHED_BOOL, false
        )
    }

    override fun isUserLoggedIn(): Boolean {
        return !TextUtils.isEmpty(
            preferenceHelper.getValue(Constants.PREF_LOGIN_DATA, "")
        )
    }

    override fun getFirebaseToken(): String {
        return preferenceHelper.getValue(Constants.PREF_FIREBASE_TOKEN, "")
    }

    override fun saveFirebaseToken(token: String) {
        preferenceHelper.setValue(Constants.PREF_FIREBASE_TOKEN, token)
    }

    override fun getAuthToken(): String {
        return preferenceHelper.getValue(Constants.PREF_BEARER_TOKEN, "")
    }

    override fun saveAuthToken(token: String) {
        preferenceHelper.setValue(Constants.PREF_BEARER_TOKEN, token)
    }

    override fun getUserProfile(): LoginResponse? {
        val loginResponseJson = preferenceHelper.getValue(Constants.PREF_LOGIN_DATA, "")
        if (loginResponseJson.isNotEmpty())
            return gson.fromJson(loginResponseJson, LoginResponse::class.java)
        return null
    }

    override fun saveUserProfile(loginResponse: LoginResponse) {
        preferenceHelper.setValue(Constants.PREF_LOGIN_DATA,
            gson.toJson(loginResponse))
        loginResponse.token?.let { saveAuthToken(it) }
    }

    override fun getObjectDetails(): ObjectDetailResponseModel? {
        val objectDetailsResponseJson = preferenceHelper.getValue(Constants.PREF_OBJECT_DETAILS, "")
        if (objectDetailsResponseJson.isNotEmpty())
            return gson.fromJson(objectDetailsResponseJson, ObjectDetailResponseModel::class.java)
        return null
    }

    override fun saveObjectDetails(objectDetailResponseModel: ObjectDetailResponseModel) {
        preferenceHelper.setValue(Constants.PREF_OBJECT_DETAILS,
            gson.toJson(objectDetailResponseModel))

    }

    override fun saveHuntId(id: Int) {
        preferenceHelper.setValue(Constants.PREF_HUNT_ID, id)
    }

    override fun getHuntId(): Int {
        return preferenceHelper.getValue(Constants.PREF_HUNT_ID, 0)
    }

    override fun logout(activity: Activity) {
        preferenceHelper.clearAll(
            Constants.PREF_FIREBASE_TOKEN,
            Constants.PREF_IS_INTRODUCTION_FINISHED_BOOL
        )
        activity.gotoActivity(
            LoginContainerActivity::class.java,
            clearAllActivity = true
        )
    }

    override fun saveSelectedPark(parks: Parks) {
        preferenceHelper.setValue(Constants.PREF_SELECTED_PARK, gson.toJson(parks))
    }

    override fun getSelectedPark(): Parks? {
        val parkResponse = preferenceHelper.getValue(Constants.PREF_SELECTED_PARK, "")
        if (parkResponse.isNotEmpty())
            return gson.fromJson(parkResponse, Parks::class.java)
        return null
    }
}
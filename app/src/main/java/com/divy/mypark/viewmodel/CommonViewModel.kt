package com.divy.mypark.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.google.android.gms.maps.model.LatLng

class CommonViewModel(application: Application) : AndroidViewModel(application) {

    var currentLatLng = MutableLiveData<LatLng?>()
    var objectDetails = MutableLiveData<ObjectDetailResponseModel?>()

    init {
        currentLatLng.value = null
//        objectDetails.value = null
    }

    fun currentLatLong(latLng: LatLng) {
        currentLatLng.value = latLng
    }

    fun setObjectDetailsResponse(response: ObjectDetailResponseModel) {
        objectDetails.value = response
    }
}
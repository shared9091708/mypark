package com.divy.mypark.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.requestmodel.ChangePasswordRequestModel
import com.divy.mypark.api.toSingleEvent
import com.divy.mypark.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository,
) : ViewModel() {

    private val _changePasswordResponse = MutableLiveData<ApiResult<Any>>()
    val changePasswordResponse = _changePasswordResponse.toSingleEvent()

    fun callChangePasswordAPI(
        currentPassword: String,
        newPassword: String,
    ) {
        viewModelScope.launch {
            authRepository.changePassword(
                ChangePasswordRequestModel(currentPassword, newPassword, newPassword)
            ).collect {
                _changePasswordResponse.value = it
            }
        }
    }
}

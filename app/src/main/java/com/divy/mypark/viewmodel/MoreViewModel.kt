package com.divy.mypark.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.toSingleEvent
import com.divy.mypark.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoreViewModel @Inject constructor(
    private val authRepository: AuthRepository,
) : ViewModel() {

    private val _logoutResponse = MutableLiveData<ApiResult<Any>>()
    val logoutResponse = _logoutResponse.toSingleEvent()

    fun callLogoutAPI() {
        viewModelScope.launch {
            authRepository.logout().collect {
                _logoutResponse.value = it
            }
        }
    }
}

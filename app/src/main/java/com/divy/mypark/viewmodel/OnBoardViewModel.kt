package com.divy.mypark.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.requestmodel.ForgotPasswordRequestModel
import com.divy.mypark.api.requestmodel.LoginRequestModel
import com.divy.mypark.api.requestmodel.RegisterRequestModel
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.api.responsemodel.RegisterResponse
import com.divy.mypark.api.toSingleEvent
import com.divy.mypark.repository.AuthRepository
import com.divy.mypark.user.UserStateManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnBoardViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val userStateManager: UserStateManager
) : ViewModel() {

    private val _loginResponse = MutableLiveData<ApiResult<LoginResponse>>()
    val loginResponse = _loginResponse.toSingleEvent()

    fun callLoginAPI(model : LoginRequestModel) {
        viewModelScope.launch {
            authRepository.login(
                model
            ).collect {
                _loginResponse.value = it
            }
        }
    }

    private val _registerResponse = MutableLiveData<ApiResult<RegisterResponse>>()
    val registerResponse = _registerResponse.toSingleEvent()

    fun callRegisterAPI(
        firstName: String,
        email: String,
        password: String,
        confirmPassword:String,
        deviceType : String
    ) {
        viewModelScope.launch {
            authRepository.signUp(
                RegisterRequestModel(firstName, email, password,confirmPassword, deviceType)
            ).collect {
                _registerResponse.value = it
            }
        }
    }

    private val _forgotPasswordResponse = MutableLiveData<ApiResult<Any>>()
    val forgotPasswordResponse = _forgotPasswordResponse.toSingleEvent()

    fun callForgotPasswordAPI(email: String) {
        viewModelScope.launch {
            authRepository.forgotPassword(ForgotPasswordRequestModel(email))
                .collect { _forgotPasswordResponse.value = it }
        }
    }
}
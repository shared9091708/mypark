package com.divy.mypark.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.requestmodel.ObjectDetailRequestModel
import com.divy.mypark.api.requestmodel.ParkIdRequestModel
import com.divy.mypark.api.requestmodel.ParkListRequestModel
import com.divy.mypark.api.requestmodel.ParkObjectListRequestModel
import com.divy.mypark.api.requestmodel.TourListRequestModel
import com.divy.mypark.api.requestmodel.UpdateHuntRequestModel
import com.divy.mypark.api.responsemodel.AddSchavangerHuntResponseModel
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.api.responsemodel.ParkListResponseModel
import com.divy.mypark.api.responsemodel.ParkObjectListResponseModel
import com.divy.mypark.api.responsemodel.TourListResponseModel
import com.divy.mypark.api.responsemodel.UpdateHuntResponseModel
import com.divy.mypark.api.toSingleEvent
import com.divy.mypark.repository.ParksRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ParksViewModel @Inject constructor(
    private val parksRepository: ParksRepository,
) : ViewModel() {

    private val _parkListResponse = MutableLiveData<ApiResult<ParkListResponseModel>>()
    val parkListResponse = _parkListResponse.toSingleEvent()

    fun callParkListApi(
        parkListRequestModel: ParkListRequestModel
    ) {
        viewModelScope.launch {
            parksRepository.getParkList(
                parkListRequestModel
            ).collect {
                _parkListResponse.value = it
            }
        }
    }


    private val _addScavengerHuntResponse =
        MutableLiveData<ApiResult<AddSchavangerHuntResponseModel>>()
    val addScavengerHuntResponse = _addScavengerHuntResponse.toSingleEvent()

    fun callAddHuntApi(
        parkIdRequestModel: ParkIdRequestModel
    ) {
        viewModelScope.launch {
            parksRepository.addScavengerHunt(
                parkIdRequestModel
            ).collect {
                _addScavengerHuntResponse.value = it
            }
        }
    }


    private val _parkObjectListResponse =
        MutableLiveData<ApiResult<ParkObjectListResponseModel>>()
    val parkObjectListResponse = _parkObjectListResponse.toSingleEvent()

    fun callParkObjectList(
        parkObjectListRequestModel: ParkObjectListRequestModel
    ) {
        viewModelScope.launch {
            parksRepository.parkObjectList(
                parkObjectListRequestModel
            ).collect {
                _parkObjectListResponse.value = it
            }
        }
    }

    private val _objectDetailResponse =
        MutableLiveData<ApiResult<ObjectDetailResponseModel>>()
    val objectDetailResponse = _objectDetailResponse.toSingleEvent()

    fun callObjectDetailApi(
        objectDetailRequestModel: ObjectDetailRequestModel
    ) {
        viewModelScope.launch {
            parksRepository.objectDetail(
                objectDetailRequestModel
            ).collect {
                _objectDetailResponse.value = it
            }
        }
    }


    private val _updateHuntResponse =
        MutableLiveData<ApiResult<UpdateHuntResponseModel>>()
    val updateHuntResponse = _updateHuntResponse.toSingleEvent()

    fun callUpdateHuntApi(
        updateHuntRequestModel: UpdateHuntRequestModel
    ) {
        viewModelScope.launch {
            parksRepository.updateHunt(
                updateHuntRequestModel
            ).collect {
                _updateHuntResponse.value = it
            }
        }
    }

    private val _tourListResponse =
        MutableLiveData<ApiResult<TourListResponseModel>>()
    val tourListResponse = _tourListResponse.toSingleEvent()

    fun callTourListApi(
        tourListRequestModel: TourListRequestModel
    ) {
        viewModelScope.launch {
            parksRepository.tourList(
                tourListRequestModel
            ).collect {
                _tourListResponse.value = it
            }
        }
    }
}

package com.divy.mypark.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.R
import com.divy.mypark.adapter.BookingTourAdapter
import com.divy.mypark.api.requestmodel.TourListRequestModel
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.api.responsemodel.Tours
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.databinding.FragmentBookingListBinding
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.viewmodel.CommonViewModel
import com.divy.mypark.viewmodel.ParksViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class BookingListFragment : BaseFragment(), View.OnClickListener {
    lateinit var binding: FragmentBookingListBinding

    @Inject
    lateinit var userStateManager: UserStateManager
    private val commonViewModel by activityViewModels<CommonViewModel>()
    private val parkViewModel by activityViewModels<ParksViewModel>()

    private var selectedPark: Parks? = null

    private var pageIndex = 1
    private var pageLimit = 10
    private var mIsLoadMoreRequested = false

    lateinit var adapter : BookingTourAdapter

    var tourList: ArrayList<Tours> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentBookingListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedPark = userStateManager.getSelectedPark()
        initView()
        observers()
    }


    private fun initView(){

        binding.tvParkName.text = selectedPark?.parkName+" "+getString(R.string.guided_tours)
        parkViewModel.callTourListApi(TourListRequestModel(selectedPark?.parkId.toString(),pageIndex.toString(),pageLimit.toString()))

        mIsLoadMoreRequested = false

        adapter = BookingTourAdapter(::onBookClick,::onMoreInfoClick,tourList)
        binding.rvGuide.adapter = adapter
        adapter.notifyDataSetChanged()

        binding.rvGuide.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mIsLoadMoreRequested) {
                        pageIndex += 1
                        parkViewModel.callTourListApi(TourListRequestModel(selectedPark?.parkId.toString(),pageIndex.toString(),pageLimit.toString()))
                    }
                }
            }
        })
    }

    private fun observers(){
        parkViewModel.tourListResponse.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                manageApiResult(it, failureListener = {
                    binding.tvNoData.visibility = View.VISIBLE
                    tourList.clear()
                    adapter.notifyDataSetChanged()
                }) { response, message ->

                    if (pageIndex == 1){
                        tourList.clear()
                    }
                    binding.tvNoData.visibility = View.GONE
                    tourList.addAll(response.tours)
                    mIsLoadMoreRequested = adapter.itemCount != response.count
                    binding.rvGuide.scrollToPosition(adapter.itemCount)
                    adapter.notifyDataSetChanged()
                }
            }
        }


    }
    private fun onBookClick(tours: Tours){

    }

    private fun onMoreInfoClick(tours: Tours){

    }
    override fun onClick(p0: View?) {
        when (p0) {

        }
    }
}
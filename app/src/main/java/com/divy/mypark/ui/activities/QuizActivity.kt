package com.divy.mypark.ui.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import com.divy.mypark.R
import com.divy.mypark.adapter.QuizAdapter
import com.divy.mypark.api.requestmodel.UpdateHuntRequestModel
import com.divy.mypark.api.responsemodel.ObjectDetailResponseModel
import com.divy.mypark.base.BaseActivity
import com.divy.mypark.base.BaseApplication
import com.divy.mypark.custom.disable
import com.divy.mypark.custom.enable
import com.divy.mypark.custom.hide
import com.divy.mypark.custom.show
import com.divy.mypark.databinding.ActivityQuizBinding
import com.divy.mypark.model.QuizModel
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.viewmodel.ParksViewModel
import com.bumptech.glide.Glide
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class QuizActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityQuizBinding

    private val parkViewModel by viewModels<ParksViewModel>()
    var  objectDetails : ObjectDetailResponseModel? = null

    private val quizList: ArrayList<QuizModel> = arrayListOf()
    lateinit var adapter: QuizAdapter
    var correctAnswer: String = ""

    @Inject
    lateinit var userStateManager: UserStateManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuizBinding.inflate(layoutInflater)
        setContentView(binding.root)

        observers()
        initView()
    }

    private fun initView() {
         objectDetails = userStateManager.getObjectDetails()

        correctAnswer = objectDetails?.objectIndigenousName.toString()


        Glide.with(this).load(objectDetails?.objectImage)
            .into(binding.ivObjectImage)

        quizList.add(QuizModel(objectDetails?.objectName.toString(), R.color.colorPrimary3, true))
        quizList.add(
            QuizModel(
                objectDetails?.objectIndigenousName.toString(),
                R.color.colorPrimary3,
                true
            )
        )
        quizList.add(QuizModel(objectDetails?.incorrectAns.toString(), R.color.colorPrimary3, true))

        quizList.shuffle()

        adapter = QuizAdapter(quizList, ::onClickOfAnswer)
        binding.rvQuiz.adapter = adapter
        adapter.notifyDataSetChanged()

        binding.apply {
            btnNext.setOnClickListener(this@QuizActivity)
            btnTryAgain.setOnClickListener(this@QuizActivity)
            ivBack.setOnClickListener(this@QuizActivity)
            btnNext.disable()
        }
    }

    private fun onClickOfAnswer(answer: String, position: Int) {
        Log.e("ClickAnswer", "onClickOfAnswer: ")
        quizList.forEach {
            it.isClickEnabled = false
        }
        if (answer == correctAnswer) {
            binding.btnNext.enable()
            binding.btnNext.show()
            binding.btnTryAgain.hide()
            binding.btnTryAgain.disable()
            quizList[position].color = R.color.correctAnsColor
            Log.e("ClickAnswer", "onClickOfAnswer: correct " + Gson().toJson(quizList))
            adapter.notifyDataSetChanged()
        } else {
            binding.btnNext.disable()
            binding.btnNext.hide()
            binding.btnTryAgain.show()
            binding.btnTryAgain.enable()
            quizList[position].color = R.color.wrongAnsColor
            Log.e("ClickAnswer", "onClickOfAnswer: Not " + Gson().toJson(quizList))
            adapter.notifyDataSetChanged()
        }
    }

    private fun observers() {
        parkViewModel.updateHuntResponse.observe(this) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { response, message ->
                    finish()
                }
            }
        }
    }


    private fun returnToMain() {
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ObjectInfoActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        finish()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }

            binding.btnNext -> {
                val time =
                    BaseApplication.getStartTime()
                        ?.let { getTimeDifferenceFormatted(it,System.currentTimeMillis()) }
                parkViewModel.callUpdateHuntApi(UpdateHuntRequestModel(userStateManager.getHuntId().toString(),time,objectDetails?.parkObjectId.toString(),"1","false"))
            }

            binding.btnTryAgain -> {
                binding.btnNext.disable()
                binding.btnNext.show()
                binding.btnTryAgain.hide()
                binding.btnTryAgain.disable()
                quizList.forEach {
                    it.color = R.color.colorPrimary3
                    it.isClickEnabled = true
                }
                adapter.notifyDataSetChanged()
            }
        }
    }

    fun millisecondsToHMS(milliseconds: Long): String {
        val hours = (milliseconds / (1000 * 60 * 60)) % 24
        val minutes = (milliseconds / (1000 * 60)) % 60
        val seconds = (milliseconds / 1000) % 60

        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeDifferenceFormatted(startTime: Long, endTime: Long): String {
        val timeDifference = endTime - startTime
        return millisecondsToHMS(timeDifference)
    }
}
package com.divy.mypark.ui.activities

import android.os.Bundle
import com.divy.mypark.R
import com.divy.mypark.base.FullScreenBaseActivity
import com.divy.mypark.config.RemoteConfigManager
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.user.UserStateManager
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

@AndroidEntryPoint
class SplashActivity : FullScreenBaseActivity() {

    @Inject
    lateinit var userStateManager: UserStateManager

    @Inject
    lateinit var remoteConfigManager: RemoteConfigManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
        remoteConfigManager.checkRemoteConfig()

        Timer("splash", false).schedule(3000) {
            if (userStateManager.isUserLoggedIn()) {
                gotoActivity(SelectParkActivity::class.java, clearAllActivity = true)
            } else
//                if (userStateManager.isOnBoardingCompleted())
                {
                gotoActivity(LoginContainerActivity::class.java, clearAllActivity = true)
            }
//            else {
//                gotoActivity(OnBoardingActivity::class.java, clearAllActivity = true)
//            }
        }
    }
}
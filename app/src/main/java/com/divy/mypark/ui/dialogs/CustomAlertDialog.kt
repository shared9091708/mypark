package com.divy.mypark.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.divy.mypark.R
import com.divy.mypark.api.responsemodel.ParkObjects
import com.divy.mypark.base.BaseDialogFragment
import com.divy.mypark.databinding.ObjectInfoDialogBinding
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

class CustomAlertDialog : BaseDialogFragment(), View.OnClickListener {
    private lateinit var textToSpeech: TextToSpeech

    companion object {
        fun showAlert(
            fragment: Fragment,
            parkData: String,
            onDialogClose: () -> Unit = {}
        ) {
            val stopperAlertDialog = CustomAlertDialog()
            stopperAlertDialog.apply {
                arguments = Bundle().apply {
                    putString("gsonData", parkData)
                }
                onDialogCloseCallback = onDialogClose
            }
            stopperAlertDialog.show(
                fragment.childFragmentManager,
                CustomAlertDialog::class.java.simpleName
            )
        }

        fun showAlert(
            activity: AppCompatActivity,
            parkData: String,
            onDialogClose: () -> Unit = {}
        ) {
            val stopperAlertDialog = CustomAlertDialog()
            stopperAlertDialog.apply {
                arguments = Bundle().apply {
                    putString("gsonData", parkData)
                }
                onDialogCloseCallback = onDialogClose
            }
            stopperAlertDialog.show(
                activity.supportFragmentManager,
                CustomAlertDialog::class.java.simpleName
            )
        }
    }

    private lateinit var binding: ObjectInfoDialogBinding
    private var gsonData: String? = null
    var onDialogCloseCallback: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        binding = ObjectInfoDialogBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner

        textToSpeech = TextToSpeech(requireContext()) { status ->
            if (status == TextToSpeech.SUCCESS) {
                val result = textToSpeech.setLanguage(Locale.US)
                Log.e("TtoS", "initView: Success")
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    // Language is not supported or missing data
                    // Handle this scenario
                    Log.e(
                        "TtoS",
                        "initView: // Language is not supported or missing data\n" + "// Handle this scenario"
                    )
                }
            } else {
                // Initialization failed
                // Handle this scenario
                Log.e("TtoS", "initView: // Initialization failed\n" + "// Handle this scenario")
            }
        }


        gsonData = arguments?.getString("gsonData")
        val type: Type = object : TypeToken<ParkObjects?>() {}.type
        val data: ParkObjects = Gson().fromJson(gsonData, type)

        binding.apply {
            tvObjectName.text = data.objectName
            tvObjectIndigenousName.text = "(" + data.objectIndigenousName + ")"
            tvObjectInfo.text = data.objectInfo
            Glide.with(this@CustomAlertDialog).load(data.objectImage).into(ivObjectImage)
            ivClose.setOnClickListener {
                onDialogCloseCallback?.invoke()
                dismissAllowingStateLoss()
            }

            binding.ivSpeakName.setOnClickListener {
                val text = binding.tvObjectName.text.toString()
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
            }

            binding.ivSpeakIndegenousName.setOnClickListener {
                val text = binding.tvObjectIndigenousName.text.toString()
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
            }
        }
        binding.executePendingBindings()
    }


    override fun onStart() {
        super.onStart()

        val dialog = dialog

        if (dialog != null) {
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            Objects.requireNonNull(getDialog()?.window!!).setWindowAnimations(
                R.style.dialog_animation_fade
            )
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    override fun onClick(view: View?) {
        onDialogCloseCallback?.invoke()
        dismissAllowingStateLoss()
    }
}
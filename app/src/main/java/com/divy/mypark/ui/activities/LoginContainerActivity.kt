package com.divy.mypark.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.divy.mypark.R
import com.divy.mypark.base.FullScreenBaseActivity
import com.divy.mypark.databinding.ActivityLoginContainerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginContainerActivity : FullScreenBaseActivity() {
    private lateinit var binding: ActivityLoginContainerBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_container)
    }
}
package com.divy.mypark.ui.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.divy.mypark.BuildConfig
import com.divy.mypark.R
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.custom.LogOutAlertDialog
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.FragmentMoreNavigationBinding
import com.divy.mypark.ui.activities.*
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.utils.Constants
import com.divy.mypark.viewmodel.MoreViewModel

import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoreNavigationFragment : BaseFragment(), View.OnClickListener,
    LogOutAlertDialog.LogoutClickListener {

    private lateinit var binding: FragmentMoreNavigationBinding
    private val moreViewModel by viewModels<MoreViewModel>()
    @Inject
    lateinit var userStateManager: UserStateManager
    private var isLogin = true
    private var loginResponseModel: LoginResponse? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_more_navigation,
                container,
                false
            )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.clickListener = this

        isLogin = userStateManager.isUserLoggedIn()
        loginResponseModel = userStateManager.getUserProfile()
        binding.model = loginResponseModel

        isLogin = true
        binding.isLogin = isLogin

        moreViewModel.logoutResponse.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { _, message ->
                    showToast(message)
                    userStateManager.logout(requireActivity())
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val requireActivity = requireActivity()
//        if (requireActivity is MainActivity)
//            requireActivity.changeTheToolbar(false, getString(R.string.settings))
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.llEditProfile -> {
                if (isLogin) requireActivity().gotoActivity(
                    EditProfileActivity::class.java,
                    null,
                    false
                )
                else requireActivity().gotoActivity(LoginContainerActivity::class.java, null, false)
            }
            binding.tvChangePassword -> {
                requireActivity().gotoActivity(
                    ChangePasswordActivity::class.java,
                    null,
                    false
                )
            }


            binding.tvAboutUs-> {
                val bundle = bundleOf(
                    Constants.EXTRA_TITLE to getString(R.string.our_team),
                    Constants.EXTRA_LINK to BuildConfig.ABOUT_US
                )
                requireActivity().gotoActivity(
                    WebActivity::class.java,
                    bundle, false
                )
            }

            binding.tvLiveLocation->{
                requireActivity().gotoActivity(LiveLocationActivity::class.java, needToFinish = false)
            }

            binding.tvChatList->{
                requireActivity().gotoActivity(ChatListActivity::class.java, needToFinish = false)
            }

            binding.tvLogout -> showLogOutDialog()
        }
    }

    private fun showLogOutDialog() {
        val logOutDialog =
            LogOutAlertDialog.newInstance(getString(R.string.are_you_sure_you_want_to_logout))
        logOutDialog.setListener(this)
        logOutDialog.show(childFragmentManager, "Log-out")
    }

    override fun onLogOutClick() {
        moreViewModel.callLogoutAPI()
    }
}
package com.divy.mypark.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.databinding.FragmentWelcomeMainBinding
import com.divy.mypark.user.UserStateManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WelcomeFragmentMain : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentWelcomeMainBinding

    @Inject
    lateinit var userStateManager: UserStateManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentWelcomeMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.apply {
            btnLogin.setOnClickListener(this@WelcomeFragmentMain)
            btnRegister.setOnClickListener(this@WelcomeFragmentMain)
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.btnLogin -> {
                findNavController().navigate(WelcomeFragmentMainDirections.actionWelcomeFragmentToLoginFragment(0))
            }

            binding.btnRegister -> {
                findNavController().navigate(WelcomeFragmentMainDirections.actionWelcomeFragmentToRegisterFragment(0))
            }
        }
    }
}
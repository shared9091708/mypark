package com.divy.mypark.ui.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.provider.MediaStore
import android.text.TextUtils
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.divy.mypark.BuildConfig
import com.divy.mypark.R
import com.divy.mypark.api.ApiResult
import com.divy.mypark.api.Event
import com.divy.mypark.api.responsemodel.LoginResponse
import com.divy.mypark.base.BaseActivity
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.ActivityEditProfileBinding
import com.divy.mypark.listeners.OnItemClickListener
import com.divy.mypark.model.StateGenericModel
import com.divy.mypark.model.StateModel
import com.divy.mypark.model.UpdateProfileRequestModel
import com.divy.mypark.ui.dialogs.SelectMediaBottomSheetDialog
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.utils.Constants
import com.divy.mypark.utils.DateTimeUtils
import com.divy.mypark.utils.getTempFile
import com.divy.mypark.viewmodel.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.quality
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class EditProfileActivity : BaseActivity(), View.OnClickListener, OnItemClickListener<Int> {

    @Inject
    lateinit var userStateManager: UserStateManager

    private lateinit var binding: ActivityEditProfileBinding
    private lateinit var myCalendar: Calendar
    private var currentPhotoPath: String? = null
    private var cameraImageFile: File? = null

    private val profileViewModel by viewModels<ProfileViewModel>()
    private var stateList: ArrayList<StateGenericModel>? = null
    private var districtList: ArrayList<StateGenericModel>? = null
    private var isNoStateData = false
    private var stateId: Int? = null
    private var cityId: Int? = null
    private var loginResponseModel: LoginResponse? = null

    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.GetContent()) {
        if (it != null) {
            profileViewModel.onGalleryImagePicked(it)
        }
    }

    private val cameraLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) {
        if (it) {
            cameraImageFile?.let { file ->
                profileViewModel.onCameraImagePicked(file)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        initView()

        profileViewModel.imageBitmapLiveData.observe(this, {
            binding.civProfilePic.setImageBitmap(it)
        })
        profileViewModel.imageFileLiveData.observe(this) {
            currentPhotoPath = it?.absolutePath
        }
    }

    private fun initView() {
        binding.clickListener = this
        binding.toolbarProfile.tvTitle.text = getString(R.string.edit_profile)
        binding.toolbarProfile.ivBack.setOnClickListener(this)
        myCalendar = Calendar.getInstance()

        loginResponseModel = userStateManager.getUserProfile()
        binding.model = loginResponseModel
        profileViewModel.stateResponse.observe(this, ::handleStateResponse)
        profileViewModel.updateProfileResponse.observe(this, ::handleUpdateProfileResponse)

//        stateId = loginResponseModel?.stateData?.stateId
//        cityId = loginResponseModel?.cityData?.cityId
    }

    private fun handleStateResponse(event: Event<ApiResult<List<StateModel>>>?) {
        event?.getContentIfNotHandled()?.let { response ->
            manageApiResult(response) { it, message ->

                if (!it.isNullOrEmpty()) {
                    if (stateList == null)
                        stateList = ArrayList()
                    for (state in it)
                        stateList?.add(StateGenericModel(state.stateId, state.name))
                }
                showStatesListSelection()
            }
        }
    }


    private fun handleUpdateProfileResponse(event: Event<ApiResult<LoginResponse>>?) {
        event?.getContentIfNotHandled()?.let { response ->
            manageApiResult(response) { it, message ->

                it.token = loginResponseModel?.token!!
                it.deviceToken = loginResponseModel?.deviceToken!!
                it.deviceType = loginResponseModel?.deviceType!!
            }
        }
    }

    private fun showStatesListSelection() {
        if (!stateList.isNullOrEmpty()) {
            val intent = Intent(this, StatePickerActivity::class.java)
            intent.putParcelableArrayListExtra(StatePickerActivity.OPTIONS_DATA, stateList)
            intent.putExtra(StatePickerActivity.OPTIONS_TITLE, getString(R.string.select_state))
            startActivityForResult(intent, Constants.REQUESTCODE_SELECT_STATE)
        } else getStateData()
    }

    private fun showDistrictPicker() {
        if (binding.tiState.editText?.text.toString().isEmpty()) {
            showToast(getString(R.string.please_select_state))
            return
        }
        if (!districtList.isNullOrEmpty()) {
            isNoStateData = false
            val intent = Intent(this, StatePickerActivity::class.java)
            intent.putParcelableArrayListExtra(StatePickerActivity.OPTIONS_DATA, districtList)
            intent.putExtra(StatePickerActivity.OPTIONS_TITLE, getString(R.string.select_district))
            startActivityForResult(intent, Constants.REQUESTCODE_SELECT_CITY)
        } else {
            isNoStateData = true
        }
    }

    private fun getStateData() {
        profileViewModel.getStateLists()
    }


    private fun openDatePickerDialog() {
        val datePickerDialog = DatePickerDialog(
            this, dateListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    internal var dateListener: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val date = DateTimeUtils.ddMMMYY.format(myCalendar.time)
            binding.tiDob.editText?.setText(date)
        }

    override fun onClick(view: View?) {
        when (view) {
            binding.toolbarProfile.ivBack -> onBackPressed()
            binding.tiDob, binding.tiDob.editText -> openDatePickerDialog()
            binding.ivEditProfilePic -> {
                showImagePickerBottomDialog()
            }
            binding.tiState, binding.tiState.editText -> showStatesListSelection()
//            binding.tiDistrict, binding.tiDistrict.editText -> showDistrictPicker()
            binding.btnSave -> handleValidationsAndUpdateProfile()
        }
    }

    private fun showImagePickerBottomDialog() {
        val dialog = SelectMediaBottomSheetDialog()
        dialog.setImageChooseListener(this)
        dialog.show(supportFragmentManager, "")
    }

    override fun onItemClick(view: View?, obj: Int, position: Int) {
        if (obj == Constants.PICK_IMAGE_FROM_CAMERA)
            openCamera()
        else openGallery()
    }

    private fun openCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            cameraImageFile = getTempFile()
            cameraImageFile?.let {
                val imageUri = FileProvider.getUriForFile(
                    this,
                    "${BuildConfig.APPLICATION_ID}.fileprovider",
                    it
                )
                cameraLauncher.launch(imageUri)
            }
        }
    }

    private fun openGallery() {
        galleryLauncher.launch("image/*")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.REQUESTCODE_SELECT_STATE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val model =
                        data?.getParcelableExtra<Parcelable>(StatePickerActivity.OPTIONS_DATA) as StateGenericModel
                    binding.tiState.editText?.setText(model.title)
                    stateId = model.id
                    cityId = null
//                    binding.tiDistrict.editText?.setText("")
                }
            }
            Constants.REQUESTCODE_SELECT_CITY -> {
                if (resultCode == Activity.RESULT_OK) {
                    val model =
                        data?.getParcelableExtra<Parcelable>(StatePickerActivity.OPTIONS_DATA) as StateGenericModel
//                    binding.tiDistrict.editText?.setText(model.title)
                    cityId = model.id
                }
            }
        }
    }


    private fun handleValidationsAndUpdateProfile() {
        binding.tiFirstName.error = null
        binding.tiLastName.error = null
        binding.tiState.error = null
        binding.tiDob.error = null

        val requestModel = UpdateProfileRequestModel(
            firstName = binding.tiFirstName.editText?.text.toString().trim(),
            lastName = binding.tiLastName.editText?.text.toString().trim(),
            dateOfBirth = binding.tiDob.editText?.text.toString().trim(),
            userMobile = binding.tiMobileNumber.editText?.text.toString().trim(),
            stateId = stateId,
            cityId = cityId
        )

        if (requestModel.firstName.isNullOrEmpty()) {
            binding.tiFirstName.error = getString(R.string.enter_valid_first_name)
            binding.scroll.smoothScrollTo(0, binding.scroll.top)
        } else if (requestModel.lastName.isNullOrEmpty()) {
            binding.tiLastName.error = getString(R.string.validation_last_name)
            binding.scroll.smoothScrollTo(0, binding.scroll.top)
        } else if (requestModel.stateId == null) {
            binding.scroll.smoothScrollTo(0, 700)
            binding.tiState.error = getString(R.string.validation_empty_state)
        } else if (requestModel.dateOfBirth.isNullOrEmpty())
            binding.tiDob.error = getString(R.string.validation_empty_dob)
        else {

            lifecycleScope.launch {
                if (!TextUtils.isEmpty(currentPhotoPath)) {
                    val compressedDocumentPath =
                        Compressor.compress(
                            this@EditProfileActivity,
                            File(currentPhotoPath),
                            Dispatchers.Main
                        ) {
                            quality(60)
                        }

                    requestModel.profilePhoto = compressedDocumentPath.absolutePath

                }
            }
        }
    }
}
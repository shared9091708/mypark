package com.divy.mypark.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.databinding.FragmentWelcomeBinding
import com.divy.mypark.ui.activities.LiveLocationActivity


class WelcomeFragment : BaseFragment(), View.OnClickListener {

    lateinit var binding: FragmentWelcomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWelcomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.clickListener = this
    }

    override fun onClick(p0: View?) {
        when (p0) {
            binding.btnNext -> {
                activity?.gotoActivity(LiveLocationActivity::class.java, needToFinish = false)
            }
        }
    }
}
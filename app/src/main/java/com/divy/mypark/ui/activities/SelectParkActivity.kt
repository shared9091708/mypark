package com.divy.mypark.ui.activities

import android.Manifest

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.divy.mypark.R
import com.divy.mypark.adapter.ParkListAdapter
import com.divy.mypark.api.requestmodel.ParkListRequestModel
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.base.BaseActivity
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.custom.hide
import com.divy.mypark.custom.show
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.ActivitySelectParkBinding
import com.divy.mypark.location.LocationUtility
import com.divy.mypark.ui.dialogs.LogoutDialog
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.utils.Constants
import com.divy.mypark.utils.Utils
import com.divy.mypark.viewmodel.CommonViewModel
import com.divy.mypark.viewmodel.ParksViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class SelectParkActivity : BaseActivity() {
    lateinit var binding: ActivitySelectParkBinding

    @Inject
    lateinit var userStateManager: UserStateManager

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val parkViewModel by viewModels<ParksViewModel>()
    private val commonViewModel by viewModels<CommonViewModel>()


    lateinit var adapter: ParkListAdapter
    val listOfParks = arrayListOf<Parks>()


    private var pageIndex = 1
    private var pageLimit = 10
    private var mIsLoadMoreRequested = false

    private var lat: String? = null
    private var long: String? = null
    private var isYesPressedForPermission = false

    private val locationManager: LocationManager by lazy {
        getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectParkBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        if (!Utils.isNetworkAvailable()) {
            showToast(getString(R.string.no_network_available))
        }

        mLocationRequest = LocationRequest
            .Builder(Priority.PRIORITY_HIGH_ACCURACY, TimeUnit.SECONDS.toMillis(1)).
            apply {
                setIntervalMillis(TimeUnit.SECONDS.toMillis(1))
                setPriority(Priority.PRIORITY_HIGH_ACCURACY)
                setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
                setMinUpdateDistanceMeters(100000F)
                setWaitForAccurateLocation(true)
            }.build()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        mIsLoadMoreRequested = false

        adapter = ParkListAdapter(::onClickOfPark, listOfParks)
        binding.rvParks.adapter = adapter
        adapter.notifyDataSetChanged()

        binding.rvParks.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mIsLoadMoreRequested) {
                        pageIndex += 1
                        Log.e("CallParkApi", "callApiAndSetData 7")
                        callApiAndSetData()
                    }
                }
            }
        })

        checkMapPermission()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (fusedLocationProviderClient != null) {
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
        }
    }

    override fun onResume() {
        super.onResume()
        if (isYesPressedForPermission)
        {
            isYesPressedForPermission = false
            checkMapPermission()
        }
//        callApiAndSetData()
        Log.e("CallParkApi", "onResume: ")
    }

    private fun onClickOfPark(park: Parks) {
//        if (park!=null){
//            showToast(getString(R.string.coming_soon))
//        }else {
            userStateManager.saveSelectedPark(park)
            val bundle = Bundle()
            bundle.putString(Constants.EXTRA_DATA, Gson().toJson(park))
            gotoActivity(
                WhatToDoActivity::class.java,
                needToFinish = false,
                clearAllActivity = false,
                bundle = bundle
            )
//        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LocationUtility.REQUEST_CHECK_SETTINGS) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // Granted
                if (!Utils.isGPSOn(this)) {
                    Utils.createLocationRequest(this)
                } else {
                    getLocation()
                }
            } else {
                //call api
                Log.e("CallParkApi", "callApiAndSetData 1")
                callApiAndSetData()
            }
        }else{
            Log.e("CallParkApi", "callApiAndSetData 100")
            callApiAndSetData()
        }
    }


    private fun checkMapPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION)
            ) {
                LogoutDialog.showAlert(
                    this,
                    getString(R.string.location_permission),
                    getString(R.string.allow_location_permission_to_get_nearest_parks_first)
                ) { boolean ->
                    if (boolean) {
                        isYesPressedForPermission = true
                        gotoSettings()
                    } else {
                        Log.e("CallParkApi", "callApiAndSetData 2")
                        callApiAndSetData()
                    }
                }
            } else {
                Utils.requestMapPermissions(this)
            }
        } else {
            if (!Utils.isGPSOn(this)) {
                Utils.createLocationRequest(this)
            } else {
                getLocation()
            }
        }
    }


    private fun gotoSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CHECK_SETTINGS_2) {
            Log.e("<Location>", "onActivityResult if")
            if (resultCode == RESULT_OK) {
                //if user allows to open gps
                getLocation()
            } else if (resultCode == RESULT_CANCELED) {
                //call api
                Log.e("CallParkApi", "callApiAndSetData 3")
                callApiAndSetData()
            }
        }
//        locationUtility.onActivityResult(requestCode,resultCode, data)
    }

    private fun callApiAndSetData() {
        val model = ParkListRequestModel(
            pageIndex.toString(),
            pageLimit.toString(),
            lat,
            long
        )
        Log.e("CallParkApi", "Api call: " + Gson().toJson(model))
        parkViewModel.callParkListApi(model)

        parkViewModel.parkListResponse.observe(this) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response, failureListener = {
                    showToast(it)
                    binding.apply {
                        rvParks.hide()
                        noParksToShow.show()
                    }
                }) { response, message ->
                    val list = response.parks
                    if (list.isNotEmpty()) {
                        binding.apply {
                            rvParks.show()
                            noParksToShow.hide()
                            if (pageIndex == 1) {
                                listOfParks.clear()
                            }
                            listOfParks.addAll(list)
                            mIsLoadMoreRequested = adapter.itemCount != response.count
                            rvParks.scrollToPosition(adapter.itemCount)
                            adapter.notifyDataSetChanged()
                        }

                    } else {
                        binding.apply {
                            rvParks.hide()
                            noParksToShow.show()
                        }
                    }
                }
            }
        }
    }

    private var mLocationRequest: LocationRequest? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    private fun getLocation() {
        showProgressDialog()
        mLocationRequest?.let {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            fusedLocationProviderClient?.requestLocationUpdates(
                it,
                locationCallback,
                Looper.getMainLooper()
            )
        }
    }

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            hideProgressDialog()
            if (locationResult != null) {
                val latitude = locationResult.locations[0]?.latitude!!
                val longitude = locationResult.locations[0]?.longitude!!

                if (latitude!=null&&longitude!=null) {
                    lat = latitude.toString()
                    long = longitude.toString()
//                    commonViewModel.currentLatLong(LatLng(latitude, longitude))
                }
                Log.e("CallParkApi", "getLocation: " + lat + " " + long)
                Log.e("CallParkApi", "callApiAndSetData 5")
                callApiAndSetData()

                if (fusedLocationProviderClient != null) {
                    fusedLocationProviderClient!!.removeLocationUpdates(this)
                }
            }else{
                Log.e("CallParkApi", "callApiAndSetData 7")
                callApiAndSetData()
            }
        }
    }


//    private fun getCurrentLocation() {
//
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            Log.e("CallParkApi", "callApiAndSetData 4")
//            callApiAndSetData()
//            return
//        }
//        val location: Location? = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
//
//        if (location != null) {
//            // Do something with the last known location
//            val latitude = location.latitude
//            val longitude = location.longitude
//            if (latitude != null && longitude != null) {
//                lat = latitude.toString()
//                long = longitude.toString()
//            }
//            Log.e("CallParkApi", "getLocation: " + lat + " " + long)
//            Log.e("CallParkApi", "callApiAndSetData 5")
//
//            callApiAndSetData()
//        } else {
//            Log.e("CallParkApi", "callApiAndSetData 6")
//            callApiAndSetData()
//        }
//
//    }
//
//    private fun getLocation2() {
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            Log.e("CallParkApi", "callApiAndSetData 4")
//
//            callApiAndSetData()
//            return
//        }
//        mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
//            val location: Location? = task.result
//            if (location != null) {
//                val geocoder = Geocoder(this, Locale.getDefault())
//                val list: List<Address> =
//                    geocoder.getFromLocation(location.latitude, location.longitude, 1)
//
//                if (list[0].latitude != null && list[0].longitude != null) {
//                    lat = list[0].latitude.toString()
//                    long = list[0].longitude.toString()
//                }
//                Log.e("CallParkApi", "getLocation: " + lat + " " + long)
//                Log.e("CallParkApi", "callApiAndSetData 5")
//
//                callApiAndSetData()
////                        mainBinding.apply {
////                            tvLatitude.text = "Latitude\n${list[0].latitude}"
////                            tvLongitude.text = "Longitude\n${list[0].longitude}"
////                            tvCountryName.text = "Country Name\n${list[0].countryName}"
////                            tvLocality.text = "Locality\n${list[0].locality}"
////                            tvAddress.text = "Address\n${list[0].getAddressLine(0)}"
////                        }
//            } else {
//                Log.e("CallParkApi", "callApiAndSetData 6")
//
//                callApiAndSetData()
//            }
//        }
//    }
}
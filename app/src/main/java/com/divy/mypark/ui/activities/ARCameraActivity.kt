package com.divy.mypark.ui.activities

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.MotionEvent
import android.view.PixelCopy
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.divy.mypark.R
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.ActivityArcameraBinding
import com.divy.mypark.user.UserStateManager
import com.google.ar.core.*
import com.google.ar.sceneform.*
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.Renderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import com.gorisse.thomas.sceneform.scene.await
import dagger.hilt.android.AndroidEntryPoint
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ARCameraActivity : AppCompatActivity(), Scene.OnUpdateListener {
    lateinit var binding: ActivityArcameraBinding
    private lateinit var arFragment: ArFragment
    private val arSceneView get() = arFragment.arSceneView
    private val scene get() = arSceneView.scene

    private var placed = false
    private var model: Renderable? = null

    @Inject
    lateinit var userStateManager: UserStateManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArcameraBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivCameraClose.setOnClickListener {
            gotoActivity(ObjectInfoActivity::class.java, needToFinish = true)
        }

        binding.ivCamera.setOnClickListener {
            takePhoto()
        }

        arFragment =
            (supportFragmentManager.findFragmentById(R.id.arFragment) as ArFragment).apply {
                setOnSessionConfigurationListener { session, config ->
                    // Modify the AR session configuration here
//                if (session.isDepthModeSupported(Config.DepthMode.AUTOMATIC)) {
//                    config.depthMode = Config.DepthMode.AUTOMATIC
//                }
//
//                config.planeFindingMode?.apply {
//                    config.planeFindingMode = Config.PlaneFindingMode.HORIZONTAL
//                }


                }
                setOnViewCreatedListener { arSceneView ->
//                arSceneView.cameraStream.depthOcclusionMode =
//                    CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_ENABLED
                    arSceneView.setFrameRateFactor(SceneView.FrameRate.FULL)
                }
//            setOnTapArPlaneListener(::onTapPlane)
            }

        lifecycleScope.launchWhenCreated {
            loadModels()
        }
    }


    private suspend fun loadModels() {
        Log.d("TAG", "loadModels: ")
        if (!userStateManager.getObjectDetails()?.androidObjectFile.isNullOrEmpty()) {
            var data = userStateManager.getObjectDetails()?.objectName?.lowercase()
            var source = "models/fiddle.glb"
            if (data?.contains("fiddle") == true) {
                source = "models/fiddle.glb"
            } else if (data?.contains("beaver") == true) {
                source = "models/beaver.glb"
            } else if (data?.contains("deer") == true) {
                source = "models/deer.glb"
            }
            model = ModelRenderable.builder()
                .setSource(this, Uri.parse(source))
                .setIsFilamentGltf(true)
                .await()

            arFragment.arSceneView.scene.addOnUpdateListener(this)
        } else {
            showToast(getString(R.string.file_not_found))
        }
    }

        private fun onTapPlane(hitResult: HitResult, plane: Plane, motionEvent: MotionEvent) {
            if (model == null /*|| modelView == null*/) {
                Toast.makeText(this, "Loading...", Toast.LENGTH_SHORT).show()
                return
            }

            // Create the Anchor.
            scene.addChild(AnchorNode(hitResult.createAnchor()).apply {
                // Create the transformable model and add it to the anchor.
                Log.d("TAG", "onTapPlane: ")
                addChild(TransformableNode(arFragment.transformationSystem).apply {
                    renderable = model
                    renderableInstance.setCulling(false)
                    renderableInstance.animate(true).start()
                    // Add the View
                    addChild(Node().apply {
                        // Define the relative position
                        localPosition = Vector3(0.0f, 1f, 0.0f)
                        localScale = Vector3(0.7f, 0.7f, 0.7f)
//                    renderable = modelView
                    })
                })
            })
        }

        /*   override fun onUpdate(frameTime: FrameTime?) {
               val frame: Frame = arFragment.getArSceneView().getArFrame() ?: return
               if (frame.camera.trackingState !== TrackingState.TRACKING) {
                   return
               }
               if (!placed) {
                   for (plane in frame.getUpdatedTrackables(Plane::class.java)) {
       //            arFragment.getPlaneDiscoveryController().hide()
                       if (plane.trackingState === TrackingState.TRACKING) {
                           for (hit in frame.hitTest(getScreenCenter().x, getScreenCenter().y)) {
                               val trackable = hit.trackable
                               if (trackable is Plane && trackable.isPoseInPolygon(hit.hitPose)) {
                                   val anchor = hit.createAnchor()
                                   val anchorNode = AnchorNode(anchor)
                                   anchorNode.parent = arFragment.getArSceneView().getScene()
                                   val pose = hit.hitPose
                                   val node = Node()
                                   node.renderable = model
                                   node.localPosition = Vector3(
                                       pose.tx(),
                                       pose.compose(Pose.makeTranslation(0.0f, 0.05f, 0.0f)).ty(),
                                       pose.tz()
                                   )
                                   node.parent = anchorNode
                               }

                           }

                       }
                       placed = true
                   }
               }else{

               }
           }*/

        private fun getScreenCenter(): Vector3 {
            val vw = findViewById<View>(android.R.id.content)
            return Vector3(vw.width / 2f, vw.height / 2f, 0f)
        }

        override fun onUpdate(frameTime: FrameTime?) {

            arFragment?.let { fragment ->
                fragment.arSceneView?.let { sceneView ->
                    sceneView.arFrame?.let { frame ->
                        Log.d("TAG", "frame: ")
                        if (!placed) {
                            val trackable = frame.getUpdatedTrackables(Plane::class.java).iterator()
                            if (trackable.hasNext()) {
                                val plane = trackable.next() as Plane
                                Log.d("TAG", "frame: " + plane.trackingState)
                                if (plane.trackingState == TrackingState.TRACKING) {
//                                fragment.planeDiscoveryController?.hide()
                                    val hitTest =
                                        frame.hitTest(
                                            frame.screenCenter().x,
                                            frame.screenCenter().y
                                        )
                                    val hitTestIterator = hitTest.iterator()
                                    if (hitTestIterator.hasNext()) {
                                        val hitResult = hitTestIterator.next()
                                        val modelAnchor = plane.createAnchor(hitResult.hitPose)
                                        val anchorNode = AnchorNode(modelAnchor)
                                        anchorNode.setParent(sceneView.scene)

                                        val transformableNode =
                                            TransformableNode(fragment.transformationSystem)
                                        transformableNode.setParent(anchorNode)
                                        transformableNode.renderable = this@ARCameraActivity.model

                                        transformableNode.worldPosition = Vector3(
                                            modelAnchor.pose.tx(),
                                            modelAnchor.pose.compose(
                                                Pose.makeTranslation(
                                                    0f,
                                                    0f,
                                                    0f
                                                )
                                            ).ty(),
                                            modelAnchor.pose.tz()
                                        )
                                        placed = true
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        private fun Frame.screenCenter(): Vector3 {
            val vw = findViewById<View>(android.R.id.content)
            return Vector3(vw.width / 2f, vw.height / 2f, 0f)
        }

    override fun onPause() {
        super.onPause()
        placed = false
    }

        private fun generateFilename(): String {
            val date: String =
                SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())
            return (Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
                .toString() + File.separator).toString() + getString(R.string.app_name) + "/" + date + "_screenshot.jpg"
        }

        @Throws(IOException::class)
        private fun saveBitmapToDisk(bitmap: Bitmap, filename: String) {
            val out = File(filename)
            if (!out.getParentFile()?.exists()!!) {
                out.getParentFile()?.mkdirs()
            }
            try {
                FileOutputStream(filename).use { outputStream ->
                    ByteArrayOutputStream().use { outputData ->
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputData)
                        outputData.writeTo(outputStream)
                        outputStream.flush()
                        outputStream.close()
                    }
                }
            } catch (ex: IOException) {
                throw IOException("Failed to save bitmap to disk", ex)
            }
        }

        private fun takePhoto() {
            val filename = generateFilename()
            val view = arFragment.arSceneView
            val bitmap = Bitmap.createBitmap(
                view.width, view.height,
                Bitmap.Config.ARGB_8888
            )
            val handlerThread = HandlerThread("PixelCopier")
            handlerThread.start()
            PixelCopy.request(view, bitmap, { copyResult ->
                if (copyResult === PixelCopy.SUCCESS) {
                    try {
                        saveBitmapToDisk(bitmap, filename)

                        val uri = Uri.parse("file://$filename")
                        val i = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                        i.data = uri
                        sendBroadcast(i)
                        val toast = Toast.makeText(
                            this,
                            "Picture is saved successfully", Toast.LENGTH_LONG
                        )
                        toast.show()
                    } catch (e: IOException) {
                        val toast: Toast = Toast.makeText(
                            this, e.toString(),
                            Toast.LENGTH_LONG
                        )
                        toast.show()
                        return@request
                    }
                } else {
                    val toast = Toast.makeText(
                        this,
                        "Failed to save picture!: $copyResult", Toast.LENGTH_LONG
                    )
                    toast.show()
                }
                handlerThread.quitSafely()
            }, Handler(handlerThread.looper))
        }

    }
package com.divy.mypark.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.divy.mypark.base.BaseDialogFragment
import com.divy.mypark.databinding.CustomLogoutDialogBinding
import java.util.*

class LogoutDialog : BaseDialogFragment(), View.OnClickListener {


    companion object {
        fun showAlert(
            fragment: Fragment,
            title: String,
            message: String,
            onDialogClose: (boolean:Boolean) -> Unit = {}
        ) {
            val stopperAlertDialog = LogoutDialog()
            stopperAlertDialog.apply {
                arguments = Bundle().apply {
                    putString("title", title)
                    putString("message", message)
                }
                onDialogCloseCallback = onDialogClose
            }
            stopperAlertDialog.show(
                fragment.childFragmentManager,
                LogoutDialog::class.java.simpleName
            )
        }

        fun showAlert(
            activity: AppCompatActivity,
            title: String,
            message: String,
            onDialogClose: (boolean:Boolean) -> Unit = {}
        ) {
            val stopperAlertDialog = LogoutDialog()
            stopperAlertDialog.apply {
                arguments = Bundle().apply {
                    putString("title", title)
                    putString("message", message)
                }
                onDialogCloseCallback = onDialogClose
            }
            stopperAlertDialog.show(
                activity.supportFragmentManager,
                LogoutDialog::class.java.simpleName
            )
        }
    }

    private lateinit var binding: CustomLogoutDialogBinding
    private var title: String? = null
    private var message: String? = null
    var onDialogCloseCallback: ((boolean: Boolean) -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        binding = CustomLogoutDialogBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.clickListener = this

        title = arguments?.getString("title")
        message = arguments?.getString("message")

        binding.title = title
        binding.description = message
        binding.executePendingBindings()
    }


    override fun onStart() {
        super.onStart()

        val dialog = dialog

        if (dialog != null) {
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
//            Objects.requireNonNull(getDialog()?.window!!).setWindowAnimations(
//                R.style.dialog_animation_fade
//            )
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    override fun onClick(view: View?) {
        when(view){
            binding.no->{
                onDialogCloseCallback?.invoke(false)
                dismiss()
            }

            binding.yes->{
                onDialogCloseCallback?.invoke(true)
                dismiss()
            }
        }
    }
}
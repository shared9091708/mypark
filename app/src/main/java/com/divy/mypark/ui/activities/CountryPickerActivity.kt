package com.divy.mypark.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.divy.mypark.R
import com.divy.mypark.base.BaseActivity
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.ActivityCountryPickerBinding
import com.divy.mypark.utils.Constants
import letsnurture.countrypicker.model.CountryModel
import letsnurture.countrypicker.ui.CountryPickerFragment

class CountryPickerActivity : BaseActivity() {

    private lateinit var binding: ActivityCountryPickerBinding
    private var countryPickerFragment: CountryPickerFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_country_picker)

        binding.toolbarCountryPicker.tvTitle.text = "Select Country"
        binding.toolbarCountryPicker.ivBack.setOnClickListener { onBackPressed() }

        supportFragmentManager.executePendingTransactions()
        countryPickerFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_country_picker) as CountryPickerFragment?

        countryPickerFragment?.setCountryPickerListener(object : CountryPickerFragment.CountryPickerListener {
            override fun onInit() {
                countryPickerFragment?.setCustomLayoutItems(
                    R.layout.list_item_country,
                    letsnurture.countrypicker.R.layout.li_common_header
                )
            }

            override fun onCountrySelected(countryModel: CountryModel) {
                showToast(countryModel.name)
                setResult(RESULT_OK, intent.apply {
                    putExtra(Constants.EXTRA_COUNTRY, countryModel)
                })
            }
        })

    }
}
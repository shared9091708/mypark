package com.divy.mypark.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.text.Html
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.databinding.ActivityObjectInfoBinding
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.viewmodel.CommonViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class ObjectInfoActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityObjectInfoBinding
    private lateinit var textToSpeech: TextToSpeech

    private val commonViewModel by viewModels<CommonViewModel>()

    @Inject
    lateinit var userStateManager: UserStateManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityObjectInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        observers()
    }

    private fun observers() {

    }

    private fun initView() {
        val objectDetails = userStateManager.getObjectDetails()
        binding.apply {
            ivBack.setOnClickListener(this@ObjectInfoActivity)
            btnNext.setOnClickListener(this@ObjectInfoActivity)
            ivSpeakName.setOnClickListener(this@ObjectInfoActivity)
            ivSpeakIndegenousName.setOnClickListener(this@ObjectInfoActivity)

            if (objectDetails != null) {
                tvObjectName.text = objectDetails.objectName
                tvObjectIndigenousName.text = "( " + objectDetails.objectIndigenousName + " )"
                tvObjectInfo.text =
                    Html.fromHtml(objectDetails.objectInfo, Html.FROM_HTML_MODE_COMPACT)
                tvObjectInfo.text = objectDetails.objectInfo
                Glide.with(this@ObjectInfoActivity).load(objectDetails.objectImage)
                    .into(ivObjectImage)
            }
        }

        textToSpeech = TextToSpeech(this) { status ->
            if (status == TextToSpeech.SUCCESS) {
                val result = textToSpeech.setLanguage(Locale.US)
                Log.e("TtoS", "initView: Success")
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    // Language is not supported or missing data
                    // Handle this scenario
                    Log.e("TtoS", "initView: // Language is not supported or missing data\n" + "// Handle this scenario")
                }
            } else {
                // Initialization failed
                // Handle this scenario
                Log.e("TtoS", "initView: // Initialization failed\n" + "// Handle this scenario")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        textToSpeech.stop()
        textToSpeech.shutdown()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }

            binding.btnNext -> {
                gotoActivity(QuizActivity::class.java, needToFinish = true)
            }

            binding.ivSpeakName -> {
                val text = binding.tvObjectName.text.toString()
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
            }

            binding.ivSpeakIndegenousName -> {
                val text = binding.tvObjectIndigenousName.text.toString()
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
//        gotoActivity(ARCameraActivity::class.java, needToFinish = true)
    }
}
package com.divy.mypark.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.widget.AppCompatImageView
import com.divy.mypark.R
import com.divy.mypark.api.responsemodel.ParkObjects
import com.divy.mypark.base.BaseDialogFragment
import com.bumptech.glide.Glide
import com.google.android.material.textview.MaterialTextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class CustomDialogFragment : BaseDialogFragment() {

    companion object {
        private const val ARG_TITLE = "park_object"

        fun newInstance(title: String): CustomDialogFragment {
            val args = Bundle().apply {
                putString(ARG_TITLE, title)
            }
            val fragment = CustomDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val gsonData = arguments?.getString(ARG_TITLE)

        val type: Type = object : TypeToken<ParkObjects?>() {}.type
        val data : ParkObjects = Gson().fromJson(gsonData,type)

        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.object_info_dialog)

        val tvObjectName = dialog.findViewById<MaterialTextView>(R.id.tvObjectName)
        val tvObjectIndegousName = dialog.findViewById<MaterialTextView>(R.id.tvObjectIndigenousName)
        val ivImage = dialog.findViewById<AppCompatImageView>(R.id.ivObjectImage)

        tvObjectName.text = data.objectName
        tvObjectIndegousName.text = data.objectIndigenousName
        Glide.with(this).load(data.objectImage).into(ivImage)

        return dialog
    }
}

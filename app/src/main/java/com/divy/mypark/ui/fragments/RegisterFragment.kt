package com.divy.mypark.ui.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.divy.mypark.R
import com.divy.mypark.api.APIConstants
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.custom.checkIsNullOrBlank
import com.divy.mypark.custom.createClickableSpan
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.FragmentRegisterBinding
import com.divy.mypark.utils.Validator
import com.divy.mypark.viewmodel.OnBoardViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentRegisterBinding
    private val onBoardViewModel by activityViewModels<OnBoardViewModel>()
    private val args: RegisterFragmentArgs by navArgs()
    var from : Int = 1
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        onBoardViewModel.registerResponse.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { _, message ->
                    showToast(message)
                    Log.e("RegisterResponse", "Success from "+from)
                    if (from == 0){
                        findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment(1))
//                        findNavController().popBackStack()
                    }else{
                        findNavController().navigateUp()
                    }
                }
            }
        }
    }

    private fun initView() {
//        From 0 = WelcomeMainFragment
//        From 1 = LoginFragment
        from = args.data

        binding.apply {
            btnRegister.setOnClickListener(this@RegisterFragment)
            ivBack.setOnClickListener(this@RegisterFragment)
        }
        val customFontColorSpan =
            SpannableStringBuilder(getString(R.string.already_have_an_account))

        val registerSpan = getString(R.string.login_now).createClickableSpan {
            if (from == 0){
                findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment(1))
            }else{
                findNavController().navigateUp()
            }
        }

        registerSpan.setSpan(
            ForegroundColorSpan(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.colorPrimary2
                )
            ), 0, getString(R.string.login_now).length, 0
        )
        registerSpan.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.login_now).length, 0)

        binding.tvLogin.text = customFontColorSpan.append(" ").append(registerSpan)
        binding.tvLogin.movementMethod = LinkMovementMethod.getInstance()

//        val registerConditionSpan =
//            SpannableStringBuilder(getString(R.string.by_clicking_register_button_you_agree))
//
//        val termsSpan = getString(R.string.terms_and_condition).createClickableSpan {
//
//        }
//        termsSpan.setSpan(
//            StyleSpan(Typeface.BOLD),
//            0,
//            getString(R.string.terms_and_condition).length,
//            0
//        )
//
//        termsSpan.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.colorGreen)), 0, getString(R.string.terms_and_condition).length, 0)
//        termsSpan.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.terms_and_condition).length, 0)
//        termsSpan.setSpan(StyleSpan(Typeface.ITALIC), 0, getString(R.string.terms_and_condition).length, 0)
//
//
//        val privacySpan = getString(R.string.privacy_policy).createClickableSpan {
//
//        }
//        privacySpan.setSpan(
//            StyleSpan(Typeface.BOLD),
//            0,
//            getString(R.string.privacy_policy).length,
//            0
//        )
//
//        privacySpan.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.colorGreen)), 0, getString(R.string.privacy_policy).length, 0)
//        privacySpan.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.privacy_policy).length, 0)
//        privacySpan.setSpan(StyleSpan(Typeface.ITALIC), 0, getString(R.string.privacy_policy).length, 0)
//
//
//        binding.tvTermsAndPrivacy.text =
//            registerConditionSpan.append(" ").append(termsSpan).append(" and ").append(privacySpan)
//        binding.tvTermsAndPrivacy.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnRegister -> {
                if (isDataValid()) {
                    val firstName = binding.tieUserName.text
                    val email = binding.tieEmail.text
                    val password = binding.tiePassword.text
                    val confirmPassword = binding.tieConfirmPassword.text
                    onBoardViewModel.callRegisterAPI(
                        firstName.toString(),
                        email.toString(),
                        password.toString(),
                        confirmPassword.toString(),
                        APIConstants.DEVICE_TYPE
                    )
                }
            }

            binding.ivBack -> {
                findNavController().navigateUp()
            }
        }
    }

    private fun isDataValid(): Boolean {
        val firstName = binding.tieUserName.text
        val email = binding.tieEmail.text
        val password = binding.tiePassword.text
        val confirmPassword = binding.tieConfirmPassword.text

        binding.tiUserName.error = ""
        binding.tiEmail.error = ""
        binding.tiPassword.error = ""
        binding.tiConfirmPassword.error = ""

        return when {
            firstName.checkIsNullOrBlank() -> {
//                showToast(R.string.enter_valid_first_name)
                binding.tiUserName.error = getString(R.string.please_enter_name)
                false
            }

            email.checkIsNullOrBlank() -> {
//                showToast(R.string.enter_valid_first_name)
                binding.tiEmail.error = getString(R.string.please_enter_email_id)
                false
            }
            !Validator.isEmailValid(email) -> {
//                showToast(R.string.enter_valid_email)
                binding.tiEmail.error = getString(R.string.please_enter_valid_email)
                false
            }
            password.checkIsNullOrBlank() -> {
//                showToast(R.string.enter_valid_first_name)
                binding.tiPassword.error = getString(R.string.please_enter_password)
                false
            }
            !Validator.isPasswordValid(password) -> {
//                showToast(R.string.enter_valid_password)
                binding.tiPassword.error = getString(R.string.enter_valid_password)
                false
            }

            confirmPassword.checkIsNullOrBlank() -> {
//                showToast(R.string.enter_valid_first_name)
                binding.tiConfirmPassword.error = getString(R.string.please_enter_confirm_password)
                false
            }

            confirmPassword.toString() != password.toString() -> {
//                showToast(R.string.password_confirm_password_does_not_match)
                binding.tiConfirmPassword.error = getString(R.string.password_confirm_password_does_not_match)
                false
            }
            else -> {
                true
            }
        }
    }
}
package com.divy.mypark.ui.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.divy.mypark.R
import com.divy.mypark.adapter.ParkObjectsListRvAdapter
import com.divy.mypark.api.requestmodel.ObjectDetailRequestModel
import com.divy.mypark.api.requestmodel.ParkIdRequestModel
import com.divy.mypark.api.requestmodel.ParkObjectListRequestModel
import com.divy.mypark.api.requestmodel.UpdateHuntRequestModel
import com.divy.mypark.api.responsemodel.ParkObjects
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.base.BaseActivity
import com.divy.mypark.base.BaseApplication
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.custom.hide
import com.divy.mypark.custom.show
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.ActivityMainBinding
import com.divy.mypark.ui.dialogs.CustomAlertDialog
import com.divy.mypark.ui.fragments.HomeFragment
import com.divy.mypark.update.InAppUpdateManager
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.utils.Constants
import com.divy.mypark.viewmodel.CommonViewModel
import com.divy.mypark.viewmodel.ParksViewModel
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textview.MaterialTextView
import com.google.gson.Gson
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanIntentResult
import com.journeyapps.barcodescanner.ScanOptions
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.ResponseBody
import java.io.*
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : BaseActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding

    private var mBottomSheetBehavior: BottomSheetBehavior<MaterialCardView>? = null
    private var mBottomSheetBehaviorComplete: BottomSheetBehavior<MaterialCardView>? = null

    @Inject
    lateinit var userStateManager: UserStateManager

    private val parkViewModel by viewModels<ParksViewModel>()
    private val commonViewModel by viewModels<CommonViewModel>()
    var huntId: String = ""

    var totalTime: String = ""

    @Inject
    lateinit var inAppUpdateManager: InAppUpdateManager

    lateinit var navController: NavController
    private var selectedPark: Parks? = null

    private var parkObjectList: ArrayList<ParkObjects> = arrayListOf()
    lateinit var adapter: ParkObjectsListRvAdapter
    private var id: Int? = null

    private var isQrScanned = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initView()

        observers()

//        inAppUpdateManager.checkForUpdate(object : InAppUpdateManager.InAppUpdateListener {
//            override fun onStartUpdate() {
//
//            }
//
//            override fun onProgressUpdate(percentage: String) {
//
//            }
//
//            override fun onFinishUpdate() {
//
//            }
//        })
    }


    override fun onResume() {
        super.onResume()
        if (isQrScanned) {
            isQrScanned = false
            parkViewModel.callParkObjectList(
                ParkObjectListRequestModel(
                    selectedPark?.parkId?.toString(),
                    "1",
                    "300",
                    huntId
                )
            )
        }
    }

    private fun initView() {
//        binding.toolbar.isBackButtonHide = true
//        binding.bottomNavigation.itemIconTintList = null


        binding.toolbarMain.cMeter.setOnChronometerTickListener {
            val elapsedTime = SystemClock.elapsedRealtime() - it.base
            totalTime = formatElapsedTime(elapsedTime)
            Log.e("TimeTotal", "initView: onTick " + totalTime)
            Log.e("TimeTotal", "initView: onTick " + totalTime)
        }

        mBottomSheetBehavior = BottomSheetBehavior.from(binding.cardBottomPanel)

        mBottomSheetBehaviorComplete = BottomSheetBehavior.from(binding.cardBottomPanelComplete)
        if (mBottomSheetBehaviorComplete != null) {
            mBottomSheetBehaviorComplete!!.skipCollapsed = true
            mBottomSheetBehaviorComplete!!.isHideable = true
            mBottomSheetBehaviorComplete!!.peekHeight = 0
            binding.cardBottomPanelComplete.hide()


            mBottomSheetBehaviorComplete!!.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_HIDDEN -> {
                            binding.cardBottomPanel.hide()
                            binding.cardBottomPanelComplete.hide()
                            binding.btnStartHunt.show()
                        }
                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {

                }
            })


        }


        if (mBottomSheetBehavior != null) {
            binding.cardBottomPanel.hide()
            mBottomSheetBehavior!!.skipCollapsed = false
            mBottomSheetBehavior!!.isHideable = false
            mBottomSheetBehavior!!.peekHeight = 180

            mBottomSheetBehavior!!.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            binding.ivArrow.show()
                            binding.btnStartHunt.hide()
//                            binding.btnStopHunt.hide()
                        }

                        BottomSheetBehavior.STATE_HIDDEN -> {
                            binding.ivArrow.hide()
                            binding.btnStartHunt.show()
//                            binding.btnStopHunt.hide()
                        }

                        BottomSheetBehavior.STATE_DRAGGING -> {
//                            binding.ivArrow.show()
                            binding.btnStartHunt.hide()
//                            binding.btnStopHunt.hide()
                        }

                        else -> {
                            binding.ivArrow.hide()
                            binding.btnStartHunt.hide()
//                            binding.btnStopHunt.show()
                        }
                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {

                }

            })
        }



        selectedPark = userStateManager.getSelectedPark()

        if (selectedPark != null) {
            binding.apply {

                if (intent.extras != null) {
                    val FOR = intent.extras!!.getString(Constants.FOR, "")
                    if (FOR == "Hunt") {
                        toolbarMain.apply {
                            tvTitle.text = getString(R.string.scavenger_hunt)
                            tvSubTitle.text = selectedPark!!.parkName.toString()
                            ivInfo.setOnClickListener(this@MainActivity)

                        }
                    } else {
                        toolbarMain.apply {
                            tvTitle.text = getString(R.string.guided_tours)
                            tvSubTitle.hide()
                            ivInfo.hide()
                            linearTimer.hide()
                        }
                        btnStartHunt.hide()

                        val navHost =
                            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
                        navController = navHost!!.navController

                        val newStartDestinationId = R.id.menu_guide

                        navController.graph = navController.graph.apply {
                            startDestination = newStartDestinationId
                        }
                    }
                }

                toolbarMain.ivBack.setOnClickListener(this@MainActivity)
                btnStartHunt.setOnClickListener(this@MainActivity)
                btnStopHunt.setOnClickListener(this@MainActivity)
                ivCloseBottomPanel.setOnClickListener(this@MainActivity)
                ivCloseBottomPanelComplete.setOnClickListener(this@MainActivity)
                tvShare.setOnClickListener(this@MainActivity)
                ivScanQr.setOnClickListener(this@MainActivity)
                ivArrow.setOnClickListener(this@MainActivity)
            }
        }


//        binding.bottomNavigation.setupWithNavController(navController)
//        binding.toolbar.ivMedia.setImageResource(R.drawable.ic_toolbar_login)
//        binding.toolbar.ivMedia.setOnClickListener(this)
    }


    private fun formatElapsedTime(elapsedTime: Long): String {
        val hours = (elapsedTime / 3600000) % 24
        val minutes = (elapsedTime / 60000) % 60
        val seconds = (elapsedTime / 1000) % 60

        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }


    fun stopTimer() {
        binding.toolbarMain.cMeter.stop()
        binding.toolbarMain.linearTimer.hide()
        binding.toolbarMain.linearTitle.show()
    }


    fun startTimer() {
        binding.toolbarMain.cMeter.start()
        binding.toolbarMain.linearTimer.show()
        binding.toolbarMain.linearTitle.hide()
    }

//    fun changeTheToolbar(shouldShowMiddleImage: Boolean, title: String = "") {
//        binding.toolbar.isFromBottomMenu = shouldShowMiddleImage
//        if (!userStateManager.isUserLoggedIn())
//            binding.toolbar.ivMedia.visibility =
//                if (shouldShowMiddleImage) View.VISIBLE else View.INVISIBLE
//
//        binding.toolbar.tvTitle.text = title
//    }

//    fun changeTheTab(@IdRes id: Int) {
//        binding.bottomNavigation.selectedItemId = id
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("onActivityResult", "Activity onActivityResult if")
        if (requestCode == Constants.REQUEST_CHECK_SETTINGS_2) {
            Log.e("onActivityResult", "Activity onActivityResult if")
            if (resultCode == AppCompatActivity.RESULT_OK) {

                val navHostFragment =
                    supportFragmentManager.findFragmentById(com.divy.mypark.R.id.nav_host_fragment) as NavHostFragment?

                if (navHostFragment != null) {
                    val currentFragment =
                        navHostFragment.childFragmentManager.fragments[0]
                    // Assumes you have only one fragment in the NavHostFragment
                    // Now you have the instance of the current fragment
                    if (currentFragment is HomeFragment) {
                        val fragment: HomeFragment = currentFragment as HomeFragment
                        fragment.onActivityResult(requestCode, resultCode, data)
                    }
                }
            } else if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                showToast(getString(R.string.please_turn_on_location_and_try_again))
            }
        }
//        locationUtility.onActivityResult(requestCode,resultCode, data)
    }

    private fun observers() {
        parkViewModel.addScavengerHuntResponse.observe(this) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { response, message ->
                    response.scavengerHuntId?.let {
                        userStateManager.saveHuntId(it)
                        huntId = it.toString()
                    }
                    parkViewModel.callParkObjectList(
                        ParkObjectListRequestModel(
                            selectedPark?.parkId?.toString(),
                            "1",
                            "300",
                            huntId
                        )
                    )
                }
            }
        }
        parkViewModel.parkObjectListResponse.observe(this) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { response, message ->

                    Log.e("ParkObjectList", "observers: " + Gson().toJson(response))

                    if (response.count != null) {
                        binding.ivArrow.hide()
                        binding.btnStartHunt.hide()
//                    binding.btnStopHunt.show()
                        binding.cardBottomPanel.show()

                        parkObjectList.clear()
                        parkObjectList.addAll(response.parkObjects)
                        adapter = ParkObjectsListRvAdapter(
                            ::onClickOfInfo,
                            parkObjectList,
                            response.count!!
                        )
                        binding.rvObjects.adapter = adapter
                        adapter.notifyDataSetChanged()

                        if (mBottomSheetBehavior != null) {
                            BaseApplication.setStartTime(System.currentTimeMillis())
                            mBottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
                            startTimer()
                        }
                    } else {
                        showToast(getString(R.string.something_went_wrong_please_try_again))
                    }
                    /* parkObjectList.forEach {
                         Log.d("TAG", "observers: " + it.objectFile)
                         if(it.objectFile != null){
                             var manager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                             *//*val uri: Uri =
                                Uri.parse(it.objectFile)*//*
                            val uri =
                                Uri.parse("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf")

                            val request = DownloadManager.Request(uri)
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                            val reference: Long = manager.enqueue(request)

                        }

                    }*/


                }
            }
        }

        parkViewModel.objectDetailResponse.observe(this) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { response, message ->
                    isQrScanned = true
                    userStateManager.saveObjectDetails(response)
                    gotoActivity(ARCameraActivity::class.java, needToFinish = false)
                }
            }
        }

        parkViewModel.updateHuntResponse.observe(this) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { response, message ->
                    stopTimer()
                    binding.apply {
                        tvTime.text = totalTime

                        val congratulationsSpan =
                            SpannableStringBuilder(getString(R.string.congratulations))
                        congratulationsSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, getString(R.string.congratulations).length, 0
                        )
                        congratulationsSpan.setSpan(
                            StyleSpan(Typeface.BOLD),
                            0,
                            getString(R.string.congratulations).length,
                            0
                        )

                        val name = userStateManager.getUserProfile()?.firstName + ","
                        val userNameSpan = SpannableStringBuilder(name)
                        userNameSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, name.length, 0
                        )
                        userNameSpan.setSpan(StyleSpan(Typeface.BOLD), 0, name.length, 0)

                        val youHaveCompletedSpan =
                            SpannableStringBuilder(getString(R.string.you_have_completed_the))
                        youHaveCompletedSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, getString(R.string.you_have_completed_the).length, 0
                        )
                        youHaveCompletedSpan.setSpan(
                            StyleSpan(Typeface.NORMAL),
                            0,
                            getString(R.string.you_have_completed_the).length,
                            0
                        )

                        val parkName =
                            selectedPark?.parkName + " " + getString(R.string.schavenger_hunt_dot)
                        val parkNameSpan = SpannableStringBuilder(parkName)
                        parkNameSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, parkName.length, 0
                        )
                        parkNameSpan.setSpan(StyleSpan(Typeface.BOLD), 0, parkName.length, 0)


                        binding.tvCongratulations.text =
                            congratulationsSpan.append(" ").append(userNameSpan).append(" ")
                                .append(youHaveCompletedSpan).append(" ").append(parkNameSpan)

                        val name2 = userStateManager.getUserProfile()?.firstName + ""
                        val userNameSpan2 = SpannableStringBuilder(name2)
                        userNameSpan2.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, name2.length, 0
                        )
                        userNameSpan2.setSpan(StyleSpan(Typeface.BOLD), 0, name2.length, 0)

                        val foundAllSpan = SpannableStringBuilder(getString(R.string.found_all))
                        foundAllSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, getString(R.string.found_all).length, 0
                        )
                        foundAllSpan.setSpan(
                            StyleSpan(Typeface.NORMAL),
                            0,
                            getString(R.string.found_all).length,
                            0
                        )

                        val totalHunts = response.totalHunts.toString()
                        val countSpan = SpannableStringBuilder(totalHunts)
                        countSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, totalHunts.length, 0
                        )
                        countSpan.setSpan(StyleSpan(Typeface.BOLD), 0, totalHunts.length, 0)

                        val huntItemSpan =
                            SpannableStringBuilder(getString(R.string.schavenger_hunt_item))
                        huntItemSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, getString(R.string.schavenger_hunt_item).length, 0
                        )
                        huntItemSpan.setSpan(
                            StyleSpan(Typeface.NORMAL),
                            0,
                            getString(R.string.schavenger_hunt_item).length,
                            0
                        )

                        val parkNameIn = selectedPark?.parkName + " in:"
                        val parkNameInSpan = SpannableStringBuilder(parkNameIn)
                        parkNameInSpan.setSpan(
                            ForegroundColorSpan(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            ), 0, parkNameIn.length, 0
                        )
                        parkNameInSpan.setSpan(StyleSpan(Typeface.BOLD), 0, parkNameIn.length, 0)

                        tvFound.text = userNameSpan2.append(" ").append(foundAllSpan).append(" ")
                            .append(countSpan).append(" ").append(huntItemSpan).append(" ")
                            .append(parkNameInSpan)
                    }
                    if (mBottomSheetBehaviorComplete != null) {
                        binding.cardBottomPanelComplete.show()
                        mBottomSheetBehaviorComplete!!.state = BottomSheetBehavior.STATE_EXPANDED

                        if (mBottomSheetBehavior != null) {
                            mBottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
                        }
                    }
                }
            }
        }
    }

    private fun onClickOfInfo(parkObjects: ParkObjects) {
        CustomAlertDialog.showAlert(
            this,
            Gson().toJson(parkObjects),
        ) {

        }

    }

    private fun showCustomDialog(parkObjects: ParkObjects) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.object_info_dialog)
        dialog.setCancelable(true)

        val tvObjectName = dialog.findViewById<MaterialTextView>(R.id.tvObjectName)
        val tvObjectIndegousName =
            dialog.findViewById<MaterialTextView>(R.id.tvObjectIndigenousName)
        val ivImage = dialog.findViewById<AppCompatImageView>(R.id.ivObjectImage)

        tvObjectName.text = parkObjects.objectName
        tvObjectIndegousName.text = parkObjects.objectIndigenousName
        Glide.with(this).load(parkObjects.objectImage).into(ivImage)
        dialog.show()
    }

    override fun onClick(p0: View?) {
        when (p0) {
            binding.toolbarMain.ivBack -> {
                showToast(getString(R.string.coming_soon))
            }

            binding.toolbarMain.ivInfo -> {
                showToast(getString(R.string.coming_soon))
            }

            binding.btnStartHunt -> {
                val currentTime = SystemClock.elapsedRealtime()
                binding.toolbarMain.cMeter.base = currentTime
//                if (huntId != "") {
//                    if (parkObjectList.isNotEmpty()) {
//                        if (mBottomSheetBehavior != null) {
//                            adapter.notifyDataSetChanged()
//                            binding.cardBottomPanel.show()
//                            mBottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
//                        }
//                    } else {
//                        parkViewModel.callParkObjectList(
//                            ParkObjectListRequestModel(
//                                selectedPark?.parkId?.toString(),
//                                "1",
//                                "300",
//                                huntId
//                            )
//                        )
//                    }
//                } else {
                parkViewModel.callAddHuntApi(ParkIdRequestModel(selectedPark?.parkId?.toString()))
//                }
            }

            binding.tvShare -> {
                showToast(getString(R.string.share))
            }

            binding.ivCloseBottomPanel -> {
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
                }
            }

            binding.ivCloseBottomPanelComplete -> {
                binding.cardBottomPanel.hide()
                binding.cardBottomPanelComplete.hide()
                binding.btnStartHunt.show()
                if (mBottomSheetBehaviorComplete != null) {
                    mBottomSheetBehaviorComplete!!.state = BottomSheetBehavior.STATE_HIDDEN
                }
            }

            binding.ivArrow -> {
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }

            binding.btnStopHunt -> {
                parkViewModel.callUpdateHuntApi(
                    UpdateHuntRequestModel(
                        huntId,
                        totalTime,
                        userStateManager.getObjectDetails()?.parkObjectId.toString(),
                        "1",
                        "true"
                    )
                )
            }

            binding.ivScanQr -> {
                val options = ScanOptions()
                options.setDesiredBarcodeFormats(ScanOptions.QR_CODE)
                options.setPrompt(getString(R.string.scan_qr_))
                options.setCameraId(0) // Use a specific camera of the device
                options.setBeepEnabled(true)
                options.setBarcodeImageEnabled(true)
                barcodeLauncher.launch(options)
            }
        }
    }

    private val barcodeLauncher: ActivityResultLauncher<ScanOptions> = registerForActivityResult(
        ScanContract()
    ) { result: ScanIntentResult ->
        if (result.contents == null) {
            Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
        } else {
            val parkObjectId = result.contents
            if (!parkObjectId.isNullOrEmpty()) {
                parkViewModel.callObjectDetailApi(ObjectDetailRequestModel(parkObjectId))
            } else {
                showToast(getString(R.string.something_went_wrong_please_try_again))
            }
        }
    }

    private fun writeResponseBodyToDisk(body: ResponseBody, name: String): Boolean {
        return try {
            val path: String =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .toString() + "/MyApp"
            val dir = File(path)
            if (!dir.exists()) dir.mkdirs()
            val futureStudioIconFile = File(path, "$name.pdf") //am saving pdf file
            if (futureStudioIconFile.exists()) futureStudioIconFile.delete()
            futureStudioIconFile.createNewFile()
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize = body.contentLength()
                var fileSizeDownloaded: Long = 0
                inputStream = body.byteStream()
                outputStream = FileOutputStream(futureStudioIconFile)
                while (true) {
                    val read: Int = inputStream.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream.write(fileReader, 0, read)
                    fileSizeDownloaded += read.toLong()
                }
                outputStream.flush()
                true
            } catch (e: IOException) {
                e.printStackTrace()
                false
            } finally {
                if (inputStream != null) {
                    inputStream.close()
                }
                if (outputStream != null) {
                    outputStream.close()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
            false
        }
    }
}
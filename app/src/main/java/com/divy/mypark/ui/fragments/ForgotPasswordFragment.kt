package com.divy.mypark.ui.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.divy.mypark.R
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.custom.createClickableSpan
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.FragmentForgotPasswordBinding
import com.divy.mypark.utils.Validator
import com.divy.mypark.viewmodel.OnBoardViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentForgotPasswordBinding
    private val onBoardViewModel by activityViewModels<OnBoardViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentForgotPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onBoardViewModel.forgotPasswordResponse.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let { response ->
                manageApiResult(response) { _, message ->
                    showToast(message)
                    findNavController().navigateUp()
                }
            }
        }

        val customFontColorSpan =
            SpannableStringBuilder(getString(R.string.remember_password))

        val loginSpan = getString(R.string.login).createClickableSpan {
                findNavController().navigateUp()
        }

        loginSpan.setSpan(
            ForegroundColorSpan(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.colorPrimary2
                )
            ), 0, getString(R.string.login).length, 0
        )
        loginSpan.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.login).length, 0)

        binding.apply {
            tvLogin.text = customFontColorSpan.append(" ").append(loginSpan)
            tvLogin.movementMethod = LinkMovementMethod.getInstance()


            ivBack.setOnClickListener(this@ForgotPasswordFragment)
            btnSendLink.setOnClickListener(this@ForgotPasswordFragment)
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                findNavController().navigateUp()
            }

            binding.btnSendLink -> {
                if (isDataValid()) {
                    val email = binding.tieEmail.text
                    onBoardViewModel.callForgotPasswordAPI(
                        email.toString()
                    )
                }
            }
        }
    }

    private fun isDataValid(): Boolean {
        val email = binding.tieEmail.text

        binding.tiEmail.error = ""

        return when {
            TextUtils.isEmpty(email) -> {
//                showToast(R.string.password_error)
                binding.tiEmail.error = getString(R.string.please_enter_email_id)
                false
            }

            !Validator.isEmailValid(email) -> {
//                showToast(R.string.enter_valid_email)
                binding.tiEmail.error = getString(R.string.please_enter_valid_email)
                false
            }

            else -> {
                true
            }
        }
    }
}
package com.divy.mypark.ui.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.divy.mypark.R
import com.divy.mypark.api.APIConstants
import com.divy.mypark.api.requestmodel.LoginRequestModel
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.custom.createClickableSpan
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.FragmentLoginBinding
import com.divy.mypark.ui.activities.SelectParkActivity
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.utils.Validator
import com.divy.mypark.viewmodel.OnBoardViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentLoginBinding
    private val onBoardViewModel by activityViewModels<OnBoardViewModel>()

    @Inject
    lateinit var userStateManager: UserStateManager
    private val args: LoginFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?, ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result
                if (!token.isNullOrEmpty())
                    userStateManager.saveFirebaseToken(token)
            })

        onBoardViewModel.loginResponse.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                manageApiResult(it, failureListener = {
                    Log.e("LoginIssue", " Response Failure Message "+it)
                    showToast(it)
                }) { loginResponse, message ->
                    showToast(message)
                    Log.e("LoginIssue", " Response Success"+Gson().toJson(loginResponse))
                    userStateManager.saveUserProfile(loginResponse)
                    requireActivity().gotoActivity(
                        SelectParkActivity::class.java,
                        clearAllActivity = true
                    )
                }
            }
        }
    }

    private fun initView() {

        //        From 0 = WelcomeMainFragment
//        From 1 = RegisterFragment
        val from = args.data
        binding.apply {
            btnLogin.setOnClickListener(this@LoginFragment)
            ivBack.setOnClickListener(this@LoginFragment)
            tvForgotPassword.setOnClickListener(this@LoginFragment)
        }
        val customFontColorSpan = SpannableStringBuilder(getString(R.string.don_t_have_an_account))
        val registerSpan = getString(R.string.register_now).createClickableSpan {
            if (from==0){
                findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment(1))
            }else{
                findNavController().navigateUp()
            }
        }
        registerSpan.setSpan(
            ForegroundColorSpan(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.colorPrimary2
                )
            ), 0, getString(R.string.register_now).length, 0
        )
        registerSpan.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            getString(R.string.register_now).length,
            0
        )
        binding.tvRegister.text = customFontColorSpan.append(" ").append(registerSpan)
        binding.tvRegister.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.tvForgotPassword -> findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToForgotPasswordFragment())

            binding.btnLogin -> {
                if (isDataValid()) {
                    val email = binding.tieEmail.text
                    val password = binding.tiePassword.text
                    val model = LoginRequestModel(email = email.toString(),password = password.toString(), deviceType = APIConstants.DEVICE_TYPE, deviceToken = userStateManager.getFirebaseToken())
                    Log.e("LoginIssue", "onClick: Request"+Gson().toJson(model))
                    onBoardViewModel.callLoginAPI(
                        model
                    )
                }
            }

            binding.ivBack -> {
                findNavController().navigateUp()
            }
        }
    }

    private fun isDataValid(): Boolean {
        val email = binding.tieEmail.text
        val password = binding.tiePassword.text

        binding.tiEmail.error = ""
        binding.tiPassword.error = ""

        return when {
            TextUtils.isEmpty(email) -> {
//                showToast(R.string.password_error)
                binding.tiEmail.error = getString(R.string.please_enter_email_id)
                false
            }

            !Validator.isEmailValid(email) -> {
//                showToast(R.string.enter_valid_email)
                binding.tiEmail.error = getString(R.string.please_enter_valid_email)
                false
            }

            TextUtils.isEmpty(password) -> {
//                showToast(R.string.password_error)
                binding.tiPassword.error = getString(R.string.please_enter_password)
                false
            }

            !Validator.isPasswordValid(password) -> {
//                showToast(R.string.enter_valid_password)
                binding.tiPassword.error = getString(R.string.enter_valid_password)
                false
            }

            else -> {
                true
            }
        }
    }
}
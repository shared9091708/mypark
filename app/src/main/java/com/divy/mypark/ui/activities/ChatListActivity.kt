package com.divy.mypark.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.divy.mypark.R
import com.divy.mypark.chat.ChatPageActivity
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.databinding.ActivityChatListBinding
import com.divy.mypark.utils.Constants

class ChatListActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding : ActivityChatListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_list)

        initView()
    }

    private fun initView(){
        binding.clickListener= this
        binding.toolbar.ivBack.setOnClickListener(this)
        binding.toolbar.tvTitle.text = "Chat List"
    }

    override fun onClick(v: View?) {
        when(v){
            binding.btnUserOne->{
                val bundle = Bundle()
                bundle.putString(Constants.USER_ID,"1")
                bundle.putString(Constants.CHAT_ID,"1")
                bundle.putString(Constants.USER_NAME,"User 1")
                gotoActivity(ChatPageActivity::class.java,bundle,needToFinish = false)
            }
            binding.btnUserTwo->{
                val bundle = Bundle()
                bundle.putString(Constants.USER_ID,"2")
                bundle.putString(Constants.CHAT_ID,"1")
                bundle.putString(Constants.USER_NAME,"User 2")
                gotoActivity(ChatPageActivity::class.java,bundle,needToFinish = false)
            }
            binding.toolbar.ivBack->{
                onBackPressed()
            }
        }
    }
}
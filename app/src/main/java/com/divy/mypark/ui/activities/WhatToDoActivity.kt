package com.divy.mypark.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.divy.mypark.adapter.ParkSubCategoryListAdapter
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.databinding.ActivityWhatToDoBinding
import com.divy.mypark.utils.Constants
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class WhatToDoActivity : AppCompatActivity() {
    lateinit var binding :ActivityWhatToDoBinding
    var park : Parks?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWhatToDoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {

        val gson = intent.extras!!.getString(Constants.EXTRA_DATA)
        val type: Type = object : TypeToken<Parks>() {}.getType()
        park = Gson().fromJson(gson,type)

        val adapter = park?.let { ParkSubCategoryListAdapter(::click, it.parkSubCat) }
        binding.rvParks.adapter = adapter
        adapter?.notifyDataSetChanged()
    }
    private fun click(position:Int){
        if (position==0){
            val bundle = Bundle()
            bundle.putString(Constants.FOR,"Hunt")
            gotoActivity(MainActivity::class.java, needToFinish = true, clearAllActivity = true, bundle = bundle)
        }else{
            val bundle = Bundle()
            bundle.putString(Constants.FOR,"Book")
            gotoActivity(MainActivity::class.java, needToFinish = true, clearAllActivity = true, bundle = bundle)
        }
    }
}
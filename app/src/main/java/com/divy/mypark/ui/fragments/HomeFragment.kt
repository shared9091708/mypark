package com.divy.mypark.ui.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import com.divy.mypark.R
import com.divy.mypark.api.responsemodel.Parks
import com.divy.mypark.base.BaseFragment
import com.divy.mypark.custom.showToast
import com.divy.mypark.databinding.FragmentHomeBinding
import com.divy.mypark.location.LocationUtility
import com.divy.mypark.ui.dialogs.LogoutDialog
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.utils.Constants
import com.divy.mypark.utils.Utils
import com.divy.mypark.viewmodel.CommonViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback {
    lateinit var binding: FragmentHomeBinding

    @Inject
    lateinit var userStateManager: UserStateManager
    private val commonViewModel by activityViewModels<CommonViewModel>()

    private var selectedPark: Parks? = null
    private var googleMap: GoogleMap? = null

    private var currentLatLng: LatLng? = null
    private var lat: String? = null
    private var long: String? = null

    private var isYesPressedForPermission = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedPark = userStateManager.getSelectedPark()
//        parkList()

        initView()
        if (selectedPark!=null){
            initMap()
        }else{
            showToast(getString(R.string.something_went_wrong))
        }

//        initMap()
    }

    private fun moveCamera(latLng: LatLng){
        googleMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
               latLng, 15f
            )
        )
    }
    private fun initView(){

        if (!Utils.isNetworkAvailable()) {
            showToast(getString(R.string.no_network_available))
        }

        mLocationRequest = LocationRequest
            .Builder(Priority.PRIORITY_HIGH_ACCURACY, TimeUnit.SECONDS.toMillis(1)).
            apply {
                setIntervalMillis(TimeUnit.SECONDS.toMillis(1))
                setPriority(Priority.PRIORITY_HIGH_ACCURACY)
                setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
                setMinUpdateDistanceMeters(1000F)
                setWaitForAccurateLocation(true)
            }.build()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())

//        commonViewModel.currentLatLng.observe(viewLifecycleOwner) { latLng ->
//            if (latLng!=null){
//                currentLatLng = latLng
//                lat = latLng.latitude.toString()
//                long = latLng.longitude.toString()
//            }
//        }

        binding.fabCurrentLocation.setOnClickListener {
            if (currentLatLng!=null){
                moveCamera(currentLatLng!!)
            }
            checkMapPermission()
        }
    }

    private fun initMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.googleMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        googleMap?.uiSettings?.isCompassEnabled = false
//        googleMap?.uiSettings?.isMyLocationButtonEnabled = true

        val latLong = selectedPark?.parkLat?.toDouble()
            ?.let { selectedPark?.parkLong?.toDouble()?.let { it1 -> LatLng(it, it1) } }
        latLong?.let { moveCamera(it) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LocationUtility.REQUEST_CHECK_SETTINGS) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // Granted
                if (!Utils.isGPSOn(requireContext())) {
                    Utils.createLocationRequest(requireActivity())
                } else {
                    if (currentLatLng!=null){
                        moveCamera(currentLatLng!!)
                    }else{
                        getLocation()
                    }
                }
            } else {
                showToast(getString(R.string.please_allow_location_permission_and_try_again))
            }
        }else{
            showToast(getString(R.string.something_went_wrong_please_try_again))
        }
    }

    private fun checkMapPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                )
            ) {
                LogoutDialog.showAlert(
                    this,
                    getString(R.string.location_permission),
                    getString(R.string.allow_location_permission_to_acces_map_related_functionality)
                ) { boolean ->
                    if (boolean) {
                        isYesPressedForPermission = true
                        gotoSettings()
                    } else {
                        showToast(getString(R.string.you_need_to_allow_location_permission_to_find_you_current_location))
                    }
                }
            } else {
                Utils.requestMapPermissions(requireActivity())
            }
        } else {
            if (!Utils.isGPSOn(requireContext())) {
                Utils.createLocationRequest(requireActivity())
            } else {
                if (currentLatLng!=null){
                    moveCamera(currentLatLng!!)
                }else{
                    getLocation()
                }
            }
        }
    }


    private fun gotoSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivity(intent)
    }


    override fun onResume() {
        super.onResume()
        if (isYesPressedForPermission){
            isYesPressedForPermission = false
            checkMapPermission()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CHECK_SETTINGS_2) {
            Log.e("onActivityResult", "Fragment onActivityResult if")
            if (resultCode == AppCompatActivity.RESULT_OK) {
                //if user allows to open gps
                if (currentLatLng!=null){
                    moveCamera(currentLatLng!!)
                }else{
                    getLocation()
                }
            } else if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                showToast(getString(R.string.please_turn_on_location_and_try_again))
            }
        }
//        locationUtility.onActivityResult(requestCode,resultCode, data)
    }


    private var mLocationRequest: LocationRequest? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    private fun getLocation() {
        showProgressDialog()
        mLocationRequest?.let {
            if (ActivityCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            fusedLocationProviderClient?.requestLocationUpdates(
                it,
                locationCallback,
                Looper.getMainLooper()
            )
        }
    }

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            hideProgressDialog()
            if (locationResult != null) {
                val latitude = locationResult.locations[0]?.latitude!!
                val longitude = locationResult.locations[0]?.longitude!!

                if (latitude!=null&&longitude!=null) {
                    lat = latitude.toString()
                    long = longitude.toString()
                    currentLatLng = LatLng(latitude, longitude)
//                    commonViewModel.currentLatLong(LatLng(latitude, longitude))
                    moveCamera(LatLng(latitude, longitude))
//                    if (fusedLocationProviderClient != null) {
//                        fusedLocationProviderClient!!.removeLocationUpdates(this)
//                    }
                }
            }else{
                showToast(getString(R.string.not_able_to_find_location_please_try_again))
            }
        }
    }


    override fun onClick(p0: View?) {
        when (p0) {

        }
    }
}
package com.divy.mypark.ui.activities

import android.Manifest
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.divy.mypark.R
import com.divy.mypark.custom.gotoActivity
import com.divy.mypark.databinding.ActivityLiveLocationBinding
import com.divy.mypark.location.LatLongModel
import com.divy.mypark.location.LocationUtility
import com.divy.mypark.location.LocationUtility.Companion.REQUEST_CHECK_SETTINGS
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.Polyline
import com.google.android.material.button.MaterialButton
import com.google.firebase.database.DatabaseReference


class LiveLocationActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener, View.OnClickListener {
    private lateinit var binding: ActivityLiveLocationBinding

//    var mServiceIntent: Intent? = null
//    private var mYourService: BackgroundService? = null
//
//    private lateinit var databaseReference: DatabaseReference
//    private lateinit var childDatabaseReference: DatabaseReference
//    private var databaseChildName: String = ""
//    private var valueEventListener: ValueEventListener? = null

    //location
    private lateinit var locationUtility: LocationUtility
    private var googleMap: GoogleMap? = null
    private var currentLatlng: LatLng? = null
    private val LIVE_LOCATION_DATA = "LiveLocationData"

    lateinit var polyline: Polyline
    lateinit var marker: Marker

    //    var parks = arrayListOf(
//        LatLng(46.14228949931901, -64.77097849509167),
//        LatLng(46.14289831415877, -64.77012184069213),
//        LatLng(46.14388290698833, -64.76911475548157)
//    )
    private var parks = arrayListOf(
        LatLng(46.114530, -64.862750),
        LatLng(46.14289831415877, -64.77012184069213),
        LatLng(46.14388290698833, -64.76911475548157)
    )

    //firebase
    private var firebaseReference: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            com.divy.mypark.R.layout.activity_live_location
        )
        binding.clickListener = this
        binding.toolMain.clickListener = this
binding.toolMain.tvTitle.setText("Park Map")
//        mYourService = BackgroundService()
//        mServiceIntent = Intent(this,BackgroundService::class.java)

//        if (!isMyServiceRunning(BackgroundService::class.java)) {
//            Log.e("ServiceTesting","NotRunning")
//            startService(mServiceIntent)
//        }else{
//            Log.e("ServiceTesting","Running")
//        }
//        startService(this)
        initView()

//        Firebase.database.goOnline()
//        databaseChildName = com.app.interactivepark.BuildConfig.LIVE_LOCATION_NODE

    }

//    private fun connectToFirebaseDatabase() {
//        databaseReference = Firebase.database.reference
//        childDatabaseReference =
//            databaseReference.child(databaseChildName).child("1").child("20")
//
//        valueEventListener =
//            childDatabaseReference.addValueEventListener(object : ValueEventListener {
//                override fun onCancelled(p0: DatabaseError) {
//
//                }
//
//                override fun onDataChange(snapshot: DataSnapshot) {
//                    var data = snapshot.value as HashMap<*, *>
//                    var latitude = data["latitude"]
//                    var longitude = data["longitude"]
//                    Log.e("LiveLocationTracker","Latitude "+latitude.toString())
//                    Log.e("LiveLocationTracker","Longitude "+longitude.toString())
//
//
//                    addMarkerOfDriver(LatLng(latitude as Double, longitude as Double),"Driver")
//
//                    val destinationLocation = LatLng(latitude, longitude)
//
//                    val url = getDirectionURL(currentLatlng!!, destinationLocation, getString(com.app.interactivepark.R.string.google_maps_key))
//                    CoroutineScope(Dispatchers.IO).launch {
//                        val client = OkHttpClient()
//                        val request = Request.Builder().url(url).build()
//                        val response = client.newCall(request).execute()
//                        val data = response.body!!.string()
//
//                        val result =  ArrayList<List<LatLng>>()
//                        try{
//                            val respObj = Gson().fromJson(data,MapData::class.java)
//                            val path =  ArrayList<LatLng>()
//
//                            for (i in 0 until respObj.routes[0].legs[0].steps.size){
//                                path.addAll(decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points))
//                            }
//
//                            result.add(path)
//                        }catch (e:Exception){
//                            e.printStackTrace()
//                        }
//
//                        runOnUiThread(Runnable {
//                            val lineoption = PolylineOptions()
//                            for (i in result.indices){
//                                lineoption.addAll(result[i])
//                                lineoption.width(10f)
//                                lineoption.color(Color.BLACK)
//                                lineoption.geodesic(true)
//                            }
//                            if (polyline!=null){
//                                polyline.remove()
//                            }
//                            polyline = googleMap?.addPolyline(lineoption)!!
//                        })
//                    }
//                }
//            })
//
//    }


//    private fun deg2rad(deg: Double): Double {
//        return deg * Math.PI / 180.0
//    }
//
//    private fun rad2deg(rad: Double): Double {
//        return rad * 180.0 / Math.PI
//    }

    private fun addMarkerOfDriver(mlatLng: LatLng, name: String) {

        if (this::marker.isInitialized) {
            if (marker != null) {
                marker.remove()
            }
        }

        val markerToAdd = MarkerOptions().position(
            LatLng(
                mlatLng.latitude,
                mlatLng.longitude
            )
        ).title(name)


        marker = googleMap?.addMarker(markerToAdd)!!

        googleMap!!.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    mlatLng.latitude,
                    mlatLng.longitude
                ), 14.0f
            )
        )
//        val startPoint = Location("locationA")
//        startPoint.latitude = currentLatlng?.latitude!!
//        startPoint.longitude = currentLatlng?.longitude!!
//
//        val endPoint = Location("locationA")
//        endPoint.latitude = 22.485600
//        endPoint.longitude = 72.802200
//
//        val distance: Float = startPoint.distanceTo(endPoint)
//        Log.d("TAG", "onMapReady: $distance")
    }

    private fun getDirectionURL(origin: LatLng, dest: LatLng, secret: String): String {
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}" +
                "&destination=${dest.latitude},${dest.longitude}" +
                "&sensor=false" +
                "&mode=driving" +
                "&key=$secret"
    }

    fun decodePolyline(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val latLng = LatLng((lat.toDouble() / 1E5), (lng.toDouble() / 1E5))
            poly.add(latLng)
        }
        return poly
    }

//    fun stopService(context: Context) {
//        val stopServiceIntent = Intent(context, LocationService::class.java)
//        context.stopService(stopServiceIntent)
//    }
//
//    fun startService(context: Context) {
//        val startServiceIntent = Intent(context, LocationService::class.java)
//
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
//            context.startForegroundService(startServiceIntent)
//        } else {
//            // Pre-O behavior.
//            context.startService(startServiceIntent)
//        }
//    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }

    private fun initView() {
        binding.clickListener = this
//        firebaseReference = FirebaseDatabase.getInstance().reference
        locationUtility = LocationUtility(this)
        initMap()
    }

    private fun initMap() {
        val mapFragment =
            supportFragmentManager.findFragmentById(com.divy.mypark.R.id.googleMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        Log.e("LiveLocationLog", "Called on Map Ready")
        googleMap = map
        googleMap?.getUiSettings()?.setCompassEnabled(false)
        googleMap?.getUiSettings()?.isMyLocationButtonEnabled = false
        googleMap?.setOnCameraIdleListener(this)
//        googleMap?.setPadding(0, 0, 0, binding.layoutBottomSpotonCode.container.height)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions()
            return
        }
        googleMap?.isMyLocationEnabled = true

        locationUtility.startLocationClient()

        lifecycle.addObserver(locationUtility)
        locationUtility.currentLocation.observe(this, Observer {
            handleLocation(it)
//            connectToFirebaseDatabase()
        })
        addMarkerOfDriver(
            LatLng(
                46.143530, -64.769621
            ), "Park"
        )

    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            REQUEST_CHECK_SETTINGS
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // Granted. Start getting the location information
                initMap()
            }
        }
    }

    private fun handleLocation(location: LatLng?) {

        if (location != null) {
//            shareLocationToFireBase("1", location.latitude.toString(), location.longitude.toString())
            if (currentLatlng == null) {
                currentLatlng = location
                Log.e("LiveLocationLog", "Location Not Null")
//                parks.forEach {
//                    Log.d("TAG", "aaaaa: ${it.latitude}")
//                }
//                var distData = distance(
//                    currentLatlng?.latitude!!,
//                    currentLatlng?.longitude!!,
//                    parks.first().latitude.toDouble(),
//                    parks.first().longitude
//                )


                parks.forEach {
                    val startPoint = Location("locationA")
                    startPoint.latitude = currentLatlng?.latitude!!
                    startPoint.longitude = currentLatlng?.longitude!!
                    val endPoint = Location("locationA")

                    endPoint.latitude = it.latitude
                    endPoint.longitude = it.longitude
                    val distance = startPoint.distanceTo(endPoint).toDouble()
                    Log.d("TAG", "handleLocation: $distance")
                    if (distance <= 10.0) {
                        showDialog()
                    }

                }

//
//                polyline = googleMap?.addPolyline(PolylineOptions())!!
            }
        } else {
            Log.e("LiveLocationLog", "Location Null")
        }

    }

    private fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val theta = lon1 - lon2
        var dist = (Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + (Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta))))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60 * 1.1515
        return dist
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun showDialog() {
        var dialog = Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog)
        val window = dialog.window
        val wlp = window?.attributes

        wlp?.gravity = Gravity.CENTER
        wlp?.flags = wlp?.flags?.and(WindowManager.LayoutParams.FLAG_BLUR_BEHIND.inv())
        window?.attributes = wlp
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val ivLook = dialog.findViewById<MaterialButton>(R.id.ivLook)
        val ivBack = dialog.findViewById<MaterialButton>(R.id.ivBack)
        ivLook.setOnClickListener {
            dialog.dismiss()
        }
        ivBack.setOnClickListener {
            dialog.dismiss()

        }
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.show()

    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    override fun onCameraIdle() {

    }

    override fun onClick(v: View?) {
        when (v) {
            binding.fabCurrentLocation -> {
                if (currentLatlng != null) {
                    googleMap?.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            currentLatlng!!, 20f
                        )
                    )
                }
            }
            binding.toolMain.ivMedia -> {
                gotoActivity(HelloArActivity::class.java, needToFinish = false)
            }
        }
    }

    private fun shareLocationToFireBase(
        livelocationid: String,
        latitude: String,
        longitude: String,
    ) {
//        live loaction id for particular tracking
        var livelocData = LatLongModel(latitude.toDouble(), longitude.toDouble())

//        pass user id in child
//        var childid = UserStateManager.getUserProfile()!!.userId.toDouble().toInt().toString()

        firebaseReference!!.child(LIVE_LOCATION_DATA)
            .child(livelocationid.toString())
            .child("10")
            .setValue(livelocData)
    }
}
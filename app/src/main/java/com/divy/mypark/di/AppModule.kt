package com.divy.mypark.di

import com.divy.mypark.config.RemoteConfigManager
import com.divy.mypark.config.RemoteConfigManagerImpl
import com.divy.mypark.preference.PreferenceHelper
import com.divy.mypark.preference.PreferenceHelperImpl
import com.divy.mypark.repository.AuthRepository
import com.divy.mypark.repository.AuthRepositoryImpl
import com.divy.mypark.repository.ParksRepository
import com.divy.mypark.repository.ParksRepositoryImpl
import com.divy.mypark.repository.ProfileRepository
import com.divy.mypark.repository.ProfileRepositoryImpl
import com.divy.mypark.user.UserStateManager
import com.divy.mypark.user.UserStateManagerImpl
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun bindsAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Singleton
    @Binds
    abstract fun bindsProfileRepository(profileRepositoryImpl: ProfileRepositoryImpl): ProfileRepository

    @Singleton
    @Binds
    abstract fun bindsParksRepository(parksRepositoryImpl: ParksRepositoryImpl): ParksRepository

    @Singleton
    @Binds
    abstract fun bindsPreferenceHelper(preferenceHelperImpl: PreferenceHelperImpl): PreferenceHelper

    @Singleton
    @Binds
    abstract fun bindsUserStateManager(userStateManagerImpl: UserStateManagerImpl): UserStateManager

    @Singleton
    @Binds
    abstract fun bindsRemoteConfigManager(remoteConfigManagerImpl: RemoteConfigManagerImpl): RemoteConfigManager

    companion object {
        @Singleton
        @Provides
        fun provideFirebaseRemoteConfig(): FirebaseRemoteConfig {
            return FirebaseRemoteConfig.getInstance()
        }
    }
}
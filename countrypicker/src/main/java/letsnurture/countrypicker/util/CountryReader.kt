package letsnurture.countrypicker.util

import android.content.Context
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import letsnurture.countrypicker.model.CountriesJsonModel
import letsnurture.countrypicker.model.CountryModel
import java.io.InputStream
import java.nio.charset.Charset

object CountryReader {

    private val countries = mutableListOf<CountryModel>()
    private val commonCountries = mutableListOf<CountryModel>()

    private val gson = Gson()

    private suspend fun loadCountriesToMemory(context: Context) {
        withContext(Dispatchers.IO) {
            val countiesJson = context
                .assets
                .open("country.json")

            val commonCountriesJson = context
                .assets
                .open("common_countries.json")

            val countryListModel = gson.fromJson(
                countiesJson.readTextAndClose(),
                CountriesJsonModel::class.java
            )

            val commonCountryListModel = gson.fromJson(
                commonCountriesJson.readTextAndClose(),
                CountriesJsonModel::class.java
            )

            countries.clear()
            commonCountries.clear()

            countries.addAll(countryListModel.countryList)
            commonCountries.addAll(commonCountryListModel.countryList)
        }
    }

    suspend fun getCountries(context: Context): List<CountryModel> {
        if (countries.isEmpty()) {
            loadCountriesToMemory(context)
        }
        return countries
    }

    suspend fun getCommonCountries(context: Context): List<CountryModel> {
        if (commonCountries.isEmpty()) {
            loadCountriesToMemory(context)
        }
        return commonCountries
    }

    private fun InputStream.readTextAndClose(charset: Charset = Charsets.UTF_8): String {
        return this.bufferedReader(charset).use { it.readLines().joinToString("") }
    }
}
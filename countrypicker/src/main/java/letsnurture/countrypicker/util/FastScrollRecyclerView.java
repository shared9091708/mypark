package letsnurture.countrypicker.util;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 * Created by ln-04 on 15/3/17.
 */

public class FastScrollRecyclerView extends RecyclerView {
    public static int indWidth = 25;
    public float mScaledWidth;
    public float mScaledHeight;
    public String[] mSections;
    public float mSx;
    public float mSy;
    public String mSection;
    public boolean mShowLetter = false;
    private Context mCtx;
    private boolean mSetupThings = false;
    private Handler mListHandler;
    private LinearLayoutManager mLayoutManager;

    /**
     * Constructor for Context
     *
     * @param context Context
     */
    public FastScrollRecyclerView(Context context) {
        super(context);
        mCtx = context;
    }

    /**
     * Constructor for Context and Attributes
     *
     * @param context Context
     * @param attrs   Attriutes Set
     */
    public FastScrollRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mCtx = context;
    }

    /**
     * Constructor for Context, Attributes and Style
     *
     * @param context  Context
     * @param attrs    Attribute Set
     * @param defStyle Style
     */
    public FastScrollRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mCtx = context;
    }


    /**
     * Setter method for Layout Manager
     *
     * @param layout Layout Manager
     */
    public void setLayoutManager(LinearLayoutManager layout) {
        super.setLayoutManager(layout);
        mLayoutManager = layout;
    }

    @Override
    public void onDraw(Canvas c) {
        if (!mSetupThings && (FastScrollRecyclerViewInterface) getAdapter() != null) {
            setupThings();
        }
        super.onDraw(c);
    }

    private void setupThings() {
        //create az text data
        Set<String> sectionSet = ((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().keySet();
        ArrayList<String> listSection = new ArrayList<>(sectionSet);
        Collections.sort(listSection);
        mSections = new String[listSection.size()];
        int i = 0;
        for (String s : listSection) {
            mSections[i++] = s;
        }


        int textSize = (getHeight() / mSections.length);

        if (textSize < 30) {
            textSize = 12;
        } else if (textSize > 30) {
            textSize = 16;
        } else {
            textSize = 14;
        }
        textSize = getSpToPx(textSize);


        mScaledWidth = indWidth * mCtx.getResources().getDisplayMetrics().density;
        mScaledHeight = textSize;
        mSx = this.getWidth() - this.getPaddingRight() - (float) (1.2 * mScaledWidth);
        mSy = (float) (((this.getHeight() - (textSize * mSections.length)) / 2.0)) - textSize;
        mSetupThings = true;
    }

    private int getSpToPx(int sp) {
        return (int) (sp * getResources().getDisplayMetrics().scaledDensity);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if (x < mSx - mScaledWidth || y < mSy || y > (mSy + mScaledHeight * mSections.length)) {
                    return super.onTouchEvent(event);
                } else {
                    // We touched the index bar
                    float yy = y - mSy;
                    int currentPosition = (int) Math.floor(yy / mScaledHeight);
                    if (currentPosition < 0) {
                        currentPosition = 0;
                    }
                    if (currentPosition >= mSections.length) {
                        currentPosition = mSections.length - 1;
                    }
                    mSection = mSections[currentPosition];
                    mShowLetter = true;
                    int positionInData = 0;
                    String value = mSection.toUpperCase();
                    if (((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().containsKey(value)) {
                        positionInData = ((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().get(value);
                    }
                    mLayoutManager.scrollToPositionWithOffset(positionInData, 0);
                    FastScrollRecyclerView.this.invalidate();
                }
                break;
            }
            case MotionEvent.ACTION_MOVE: {

                if (!mShowLetter
                        && (x < mSx - mScaledWidth || y < mSy || y > (mSy + mScaledHeight * mSections.length))) {
                    return super.onTouchEvent(event);
                } else {
                    float yy = y - mSy;
                    int currentPosition = (int) Math.floor(yy / mScaledHeight);
                    if (currentPosition < 0) {
                        currentPosition = 0;
                    }
                    if (currentPosition >= mSections.length) {
                        currentPosition = mSections.length - 1;
                    }
                    mSection = mSections[currentPosition];
                    mShowLetter = true;
                    int positionInData = 0;
                    String value = mSection.toUpperCase();
                    if (((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().containsKey(value)) {

                        positionInData = ((FastScrollRecyclerViewInterface) getAdapter()).getMapIndex().get(value);

                    }

                    mLayoutManager.scrollToPositionWithOffset(positionInData, 0);
                    FastScrollRecyclerView.this.invalidate();

                }
                break;

            }
            case MotionEvent.ACTION_UP: {
                mListHandler = new ListHandler();
                mListHandler.sendEmptyMessageDelayed(0, 100);
                if (x < mSx - mScaledWidth || y < mSy || y > (mSy + mScaledHeight * mSections.length)) {
                    return super.onTouchEvent(event);
                } else {
                    return true;
                }
            }
            default:
                break;
        }
        return true;
    }

    private class ListHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mShowLetter = false;
            FastScrollRecyclerView.this.invalidate();
        }


    }
}

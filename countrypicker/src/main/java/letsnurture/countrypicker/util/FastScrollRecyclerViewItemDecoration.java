package letsnurture.countrypicker.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import androidx.recyclerview.widget.RecyclerView;

import letsnurture.countrypicker.R;

/**
 * Class for FastScrollRecyclerView Item Decoration and inflating Views
 */
public class FastScrollRecyclerViewItemDecoration extends RecyclerView.ItemDecoration {

    private Context mContext;

    /**
     * Class Constructor for context
     * @param context Context
     */
    public FastScrollRecyclerViewItemDecoration(Context context) {
        mContext = context;
    }

    @Override
    public void onDrawOver(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(canvas, parent, state);

        String[] sections = ((FastScrollRecyclerView) parent).mSections;
        String section = ((FastScrollRecyclerView) parent).mSection;
        boolean showLetter = ((FastScrollRecyclerView) parent).mShowLetter;

        int width = parent.getWidth();
        int height = parent.getHeight();
        int textSize = height / sections.length;

        if (textSize < 30) {
            textSize = 10;
        } else if (textSize > 30) {
            textSize = 14;
        } else {
            textSize = 12;
        }
        textSize = getSpToPx(textSize);

        // We draw the letter in the middle
        if (showLetter & section != null && !section.equals("")) {
            //overlay everything when displaying selected index Letter in the middle
            Paint overlayDark = new Paint();
            overlayDark.setColor(Color.BLACK);
            overlayDark.setAlpha(100);
            canvas.drawRect(0, 0, parent.getWidth(), parent.getHeight(), overlayDark);
            float middleTextSize = mContext.getResources().getDimension(R.dimen.fast_scroll_overlay_text_size);
            Paint middleLetter = new Paint();
            middleLetter.setColor(Color.WHITE);
            middleLetter.setTextSize(middleTextSize);
            middleLetter.setAntiAlias(true);
            middleLetter.setFakeBoldText(true);
            middleLetter.setStyle(Paint.Style.FILL);
            int xPos = (canvas.getWidth() - (int) middleTextSize) / 2;
            int yPos = (int) ((canvas.getHeight() / 2) - ((middleLetter.descent() + middleLetter.ascent()) / 2));


            canvas.drawText(section.toUpperCase(), xPos, yPos, middleLetter);
        }
        // draw indez A-Z

        Paint textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
        int drawHeight = textSize * sections.length;
        int marginTop = (height - drawHeight) / 2;
        int offset = marginTop;

        for (int i = 0; i < sections.length; i++) {
            if (showLetter & section != null && !section.equals("") && section != null
                    && sections[i].equalsIgnoreCase(section)) {
                textPaint.setColor(Color.WHITE);
                textPaint.setAlpha(255);
                textPaint.setFakeBoldText(true);
                canvas.drawText(sections[i].toUpperCase(),
                        width - textSize,
                        offset,
                        textPaint);
                offset += textSize;
            } else {
                textPaint.setColor(Color.BLACK);
                textPaint.setAlpha(200);
                textPaint.setFakeBoldText(false);
                canvas.drawText(sections[i].toUpperCase(),
                        width - textSize,
                        offset,
                        textPaint);
                offset += textSize;
            }
        }
    }

    private int getSpToPx(int sp) {
        return (int) (sp * mContext.getResources().getDisplayMetrics().scaledDensity);
    }
}
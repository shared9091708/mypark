package letsnurture.countrypicker.util;

import java.util.HashMap;

/**
 * Interface for Map Index for FastScrollRecyclerView
 */
public interface FastScrollRecyclerViewInterface {

    /**
     * Method which returns HashMap of MapIndex
     * @return HashMap of MapIndex
     */
    HashMap<String, Integer> getMapIndex();
}
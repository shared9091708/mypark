package letsnurture.countrypicker.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CountryModel(
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("zipCodeName")
    @Expose
    val zipCodeName: String,
    @SerializedName("extension")
    @Expose
    val extension: String
): Parcelable

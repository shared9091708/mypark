package letsnurture.countrypicker.model

data class CountryHeader(
    val title: String
)

package letsnurture.countrypicker.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CountriesJsonModel(
    @SerializedName("countryList")
    @Expose
    val countryList: List<CountryModel>
)
package letsnurture.countrypicker.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import letsnurture.countrypicker.R
import letsnurture.countrypicker.model.CountryModel
import letsnurture.countrypicker.util.FastScrollRecyclerViewInterface
import java.util.*

const val VIEW_TYPE_COUNTRY = 1
const val VIEW_TYPE_HEADER = 2

class CountryPickerAdapter(private val onCountrySelected: (CountryModel) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    FastScrollRecyclerViewInterface {

    private val listWithHeaders = mutableListOf<Any>()
    private var layoutResCountry = R.layout.li_common_country
    private var layoutResHeader = R.layout.li_common_header
    private val mapIndex = hashMapOf<String, Int>()

    fun setData(
        listWithHeaders: List<Any>,
        mapIndex: HashMap<String, Int>,
        layoutResCountry: Int, layoutResHeader: Int
    ) {
        this.listWithHeaders.clear()
        this.listWithHeaders.addAll(listWithHeaders)
        this.mapIndex.clear()
        this.mapIndex.putAll(mapIndex)
        this.layoutResCountry = layoutResCountry
        this.layoutResHeader = layoutResHeader
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (listWithHeaders[position] is String) {
            VIEW_TYPE_HEADER
        } else {
            VIEW_TYPE_COUNTRY
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_COUNTRY -> {
                return CountryViewHolder(parent, layoutResCountry)
            }
            VIEW_TYPE_HEADER -> {
                return CountryHeaderViewHolder(parent, layoutResHeader)
            }
            else -> {
                super.createViewHolder(parent, viewType)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CountryViewHolder) {
            holder.bindData(listWithHeaders[position] as CountryModel)
        } else if (holder is CountryHeaderViewHolder) {
            holder.bindData(listWithHeaders[position] as String)
        }
    }

    override fun getItemCount(): Int {
        return listWithHeaders.size
    }


    inner class CountryViewHolder(parent: ViewGroup, @LayoutRes layoutResource: Int) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(layoutResource, parent, false)
        ) {

        init {
            itemView.setOnClickListener {
                onCountrySelected(listWithHeaders[adapterPosition] as CountryModel)
            }
        }

        val tvName = itemView.findViewById<TextView>(R.id.tv_name)
        val ivCountry = itemView.findViewById<ImageView>(R.id.iv_logo)

        fun bindData(countryModel: CountryModel) {
            tvName.text = countryModel.name
            ivCountry.setImageResource(
                getCountryFlag(itemView.context, countryModel.code)
            )
        }
    }

    inner class CountryHeaderViewHolder(parent: ViewGroup, @LayoutRes layoutResource: Int) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(layoutResource, parent, false)
        ) {

        val tvTitle = itemView.findViewById<TextView>(R.id.tv_header)

        fun bindData(title: String) {
            tvTitle.text = title
        }
    }

    private fun getCountryFlag(context: Context, countryCode: String): Int {
        var resourceId: Int = context.resources.getIdentifier(
            countryCode.lowercase(Locale.US),
            "drawable",
            context.packageName
        )
        if (resourceId == 0) {
            resourceId = context.resources.getIdentifier(
                "flag_placeholder",
                "drawable",
                context.packageName
            )
        }
        return resourceId
    }

    override fun getMapIndex(): HashMap<String, Int> {
        return mapIndex
    }

}

abstract class BaseCountryViewHolder(parent: ViewGroup, @LayoutRes layoutResource: Int) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(layoutResource, parent, false)
    ) {
    abstract fun bindData(countryModel: CountryModel, position: Int)
}

abstract class BaseHeaderViewModel(parent: ViewGroup, @LayoutRes layoutResource: Int) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(layoutResource, parent, false)
    ) {
    abstract fun bindData(title: String, position: Int)
}

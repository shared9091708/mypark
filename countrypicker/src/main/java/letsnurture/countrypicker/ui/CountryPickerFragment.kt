package letsnurture.countrypicker.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import letsnurture.countrypicker.R
import letsnurture.countrypicker.databinding.FragmentCountryPickerBinding
import letsnurture.countrypicker.model.CountryModel
import letsnurture.countrypicker.util.CountryReader
import letsnurture.countrypicker.util.FastScrollRecyclerViewItemDecoration

class CountryPickerFragment: Fragment(R.layout.fragment_country_picker) {

    private lateinit var binding: FragmentCountryPickerBinding
    private val countryPickerAdapter = CountryPickerAdapter() {
        countryPickerListener?.onCountrySelected(it)
    }
    private val listWithHeaders = mutableListOf<Any>()
    private val mapIndex = hashMapOf<String, Int>()

    private var countryPickerListener: CountryPickerListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCountryPickerBinding.bind(view)

        binding.rvCountryPicker.setLayoutManager(
            LinearLayoutManager(requireContext())
        )
        lifecycleScope.launchWhenCreated {
            val countries = CountryReader.getCountries(requireContext())
            val commonCountries = CountryReader.getCommonCountries(requireContext())

            listWithHeaders.add("Common Countries")
            listWithHeaders.addAll(commonCountries)

            val groupByFirstLetter = countries.groupBy { it.name.first() }
            groupByFirstLetter.toSortedMap().forEach {
                listWithHeaders.add(it.key.toString())
                listWithHeaders.addAll(it.value)
            }

            listWithHeaders.forEachIndexed { index, any ->
                if (any is String && any.length == 1) {
                    mapIndex[any] = index
                }
            }

            countryPickerAdapter.setData(
                listWithHeaders,
                mapIndex,
                R.layout.li_common_country,
                R.layout.li_common_header
            )
            binding.rvCountryPicker.adapter = countryPickerAdapter

            val decoration =
                FastScrollRecyclerViewItemDecoration(requireContext())
            binding.rvCountryPicker.addItemDecoration(decoration)
            binding.rvCountryPicker.itemAnimator = DefaultItemAnimator()

            countryPickerListener?.onInit()
        }
    }

    fun setCustomLayoutItems(listItemCountry: Int, liCommonHeader: Int) {
        countryPickerAdapter.setData(
            listWithHeaders,
            mapIndex,
            listItemCountry,
            liCommonHeader
        )
        binding.rvCountryPicker.adapter = countryPickerAdapter
    }

    fun setCountryPickerListener(listener: CountryPickerListener) {
        this.countryPickerListener = listener
    }

    interface CountryPickerListener {
        fun onInit()
        fun onCountrySelected(countryModel: CountryModel)
    }

}